package tools.debug;

import hxd.Event;
import h2d.RenderContext;
import h2d.Flow.FlowAlign;

class Spy extends h2d.Flow {
	var updaters:Array<Void->Void> = [];

	public function new(?parent) {
		super(parent);
		this.layout = Vertical;
		this.verticalSpacing = 5;
		this.padding = 10;
		this.horizontalAlign = FlowAlign.Right;
	}

	override function onAdd() {
		super.onAdd();
		this.minWidth = this.maxWidth = getScene().width;
		this.minHeight = this.maxHeight = getScene().height;
	}

	function getFont() {
		return hxd.res.DefaultFont.get();
	}

	public function addButton(label:String, onClick:Void->Void) {
		var f = new h2d.Flow(this);
		f.padding = 5;
		f.paddingBottom = 7;
		f.backgroundTile = h2d.Tile.fromColor(0x404040);
		var tf = new h2d.Text(getFont(), f);
		tf.text = label;
		f.enableInteractive = true;
		f.interactive.cursor = Button;
		f.interactive.onClick = function(_) onClick();
		f.interactive.onOver = function(_) f.backgroundTile = h2d.Tile.fromColor(0x606060);
		f.interactive.onOut = function(_) f.backgroundTile = h2d.Tile.fromColor(0x404040);
		return f;
	}

	public function addSlider(label:String, get:Void->Float, set:Float->Void, min:Float = 0., max:Float = 1.) {
		var f = new h2d.Flow(this);

		f.horizontalSpacing = 5;

		var tf = new h2d.Text(getFont(), f);
		tf.text = label;
		tf.maxWidth = 70;
		tf.textAlign = Right;

		var sli = new h2d.Slider(100, 10, f);
		sli.minValue = min;
		sli.maxValue = max;
		sli.value = get();

		var tf = new h2d.TextInput(getFont(), f);
		tf.text = "" + hxd.Math.fmt(sli.value);
		sli.onChange = function() {
			set(sli.value);
			tf.text = "" + hxd.Math.fmt(sli.value);
			f.needReflow = true;
		};
		tf.onChange = function() {
			var v = Std.parseFloat(tf.text);
			if (Math.isNaN(v))
				return;
			sli.value = v;
			set(v);
		};
		return sli;
	}

	public function addCheck(label:String, get:Void->Bool, set:Bool->Void) {
		var f = new h2d.Flow(this);

		f.horizontalSpacing = 5;

		var tf = new h2d.Text(getFont(), f);
		tf.text = label;
		tf.maxWidth = 70;
		tf.textAlign = Right;

		var size = 10;
		var b = new h2d.Graphics(f);
		function redraw() {
			b.clear();
			b.beginFill(0x808080);
			b.drawRect(0, 0, size, size);
			b.beginFill(0);
			b.drawRect(1, 1, size - 2, size - 2);
			if (get()) {
				b.beginFill(0xC0C0C0);
				b.drawRect(2, 2, size - 4, size - 4);
			}
		}
		var i = new h2d.Interactive(size, size, b);
		i.onClick = function(_) {
			set(!get());
			redraw();
		};
		redraw();
		return i;
	}

	public function addChoice(text, choices, callb, value = 0) {
		var font = getFont();
		var i = new h2d.Interactive(110, font.lineHeight, this);
		i.backgroundColor = 0xFF808080;
		this.getProperties(i).paddingLeft = 20;

		var t = new h2d.Text(font, i);
		t.maxWidth = i.width;
		t.text = text + ":" + choices[value];
		t.textAlign = Center;

		i.onClick = function(_) {
			value++;
			value %= choices.length;
			callb(value);
			t.text = text + ":" + choices[value];
		};
		i.onOver = function(_) {
			t.textColor = 0xFFFFFF;
		};
		i.onOut = function(_) {
			t.textColor = 0xEEEEEE;
		};
		i.onOut(new Event(EventKind.EOut));
		return i;
	}

	public function addText(text = "", getUpdatedValue:Void->String = null) {
		var tf = new h2d.Text(getFont(), this);
		tf.text = text;
		if (getUpdatedValue != null)
			updaters.push(() -> tf.text = getUpdatedValue());
		return tf;
	}

	override function removeChildren() {
		super.removeChildren();
		updaters.splice(0, updaters.length);
	}

	override function sync(ctx:RenderContext) {
		super.sync(ctx);
		for (updater in updaters) {
			updater();
		}
	}
}
