package tools;

using Safety;
using Lambda;

class ObjectPool<T> {
	public var items(default, null):List<T> = new List<T>();
	public var length(default, null):Int;

	public var allocator:Void->T;

	public function new(length:Int, allocator:Void->T) {
		this.length = length;
		this.allocator = allocator;

		// create array full of instances using comprehension syntax
		for (i in 0...length)
			this.items.add(allocator());
	}

	public function getItem():T {
		if (this.items.length > 0)
			return items.pop().sure();
		else
			// pool is empty, allocate new instances
			return this.allocator();
	}

	public function putItem(item:T):Void {
		if (this.items.length < length && !this.items.has(item)) {
			this.items.push(item);
		}
	}
}
