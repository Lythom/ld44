package tools.anim;

import haxe.ds.StringMap;
import h2d.Object;
import h2d.RenderContext;
import h2d.Drawable;
import h2d.Tile;

using Safety;

/**
 * A single frame index or a list of frame indexes
 */
enum SingleOrList<T> {
	Single(v:T);
	List(v:Array<T>);
}

/**
 * Define one animation sequence
 */
typedef Sequence = {
	/**
	 * indexes of frames that composes the sequence
	 */
	framesIdx:Array<Int>,

	/**
	 * Duration of all or each frame.
	 * Indexed on animation speed.
	 * Ie. 1 = normal duration, 2 = double duration, 0.5 = half duration.
	 */
	expositions:SingleOrList<Float>,
	/**
	 * If transitionning from another Sequence, play the transition Sequence before.
	 */
	?transitionFrom:Null<StringMap<Sequence>>
};

/**
	Display an animated sequence of bitmap tiles on the screen.
**/
class AnimatedSprite extends Drawable {
	/**
		The list of frames available for this Animation.
		If the frames are empty or if a tile is frames is null, a pink 5x5 bitmap will be displayed instead. Use remove() or visible=false to hide an AnimatedSprite
	**/
	public var frames(default, null):Array<Tile>;

	/**
	 * List of sequences available to be played in this AnimatedSprite.
	 * The use of a `@:enum abstract(String) from to String { … }` is recommandes for keys.
	 */
	public var sequences:StringMap<Sequence>;

	/**
		The current frame the animation is currently playing. Always in `[0,frames.length]` range
	**/
	public var currentFrame(get, set):Float;

	/**
		The current sequence the animation is playing.
	**/
	public var currentSequence:String;

	/**
	 * If transitionning from another sequence. The previous sequence name.
	 */
	public var transitionningFrom:Null<String>;

	/**
		The speed (in frames per second) at which the animation is playing (default 15.)
	**/
	public var speed:Float;

	/**
		Setting pause will pause the animation, preventing any automatic change to currentFrame.
	**/
	public var pause:Bool = false;

	/**
		Disabling loop will stop the animation at the last frame (default : true)
	**/
	public var loop:Bool = true;

	/**
		When enable, fading will draw two consecutive frames with alpha transition between
		them instead of directly switching from one to another when it reaches the next frame.
		This can be used to have smoother animation on some effects.
	**/
	public var fading:Bool = false;

	var curFrame:Float;

	/**
		Create a new animation with the specified frames, speed and parent object
	**/
	public function new(?frames:Array<Tile>, sequences:StringMap<Sequence>, ?speed:Float, ?parent:h2d.Object) {
		super(parent);
		this.frames = frames == null ? [] : frames;
		this.sequences = sequences == null ? new StringMap<Sequence>() : sequences;
		this.curFrame = 0;
		this.speed = speed == null ? 15 : speed;
		this.currentSequence = sequences.keys().next();
	}

	inline function get_currentFrame() {
		return curFrame;
	}

	/**
		Change the currently playing animation and unset the pause if it was set.
	**/
	public function play(nextSequence:String, atFrame = 0.) {
		if (nextSequence == null || !(this.sequences.exists(nextSequence)))
			throw 'Can\'t set a non existing animation. Please declare a "${nextSequence}" key in animations.';

		// if the animation change
		if (this.currentSequence != nextSequence) {
			this.pause = false;
			this.currentFrame = atFrame;

			// We alreay checked if the animation exists, assume it's a valid animation
			this.sequences.get(nextSequence).run(nextAnim -> {
				if (nextAnim.transitionFrom != null && nextAnim.transitionFrom.sure().exists(this.currentSequence)) {
					this.transitionningFrom = this.currentSequence;
				} else {
					this.transitionningFrom = null;
				}
			});
			this.currentSequence = nextSequence;
		}
	}

	/**
		onAnimEnd is automatically called each time the animation will reach past the last frame.
		If loop is true, it is called everytime the animation loops.
		If loop is false, it is called once when the animation reachs `currentFrame == frames.length`
	**/
	public dynamic function onAnimEnd() {}

	function set_currentFrame(frame:Float) {
		var currFrames = getCurrSequenceFrames();
		curFrame = currFrames.length == 0 ? 0 : frame % currFrames.length;
		if (curFrame < 0)
			curFrame += currFrames.length;
		return curFrame;
	}

	override function getBoundsRec(relativeTo:Object, out:h2d.col.Bounds, forSize:Bool) {
		super.getBoundsRec(relativeTo, out, forSize);
		var tile = getFrame();
		if (tile != null)
			addBounds(relativeTo, out, tile.dx, tile.dy, tile.width, tile.height);
	}

	override function sync(ctx:RenderContext) {
		super.sync(ctx);
		var prev = curFrame;
		var currFrames = getCurrSequenceFrames();
		if (!pause) {
			var expo = getCurrSequenceExpo(Std.int(curFrame));
			curFrame += speed * ctx.elapsedTime / expo;
		}
		if (curFrame < currFrames.length)
			return;
		if (loop) {
			if (currFrames.length == 0)
				curFrame = 0;
			else
				curFrame %= currFrames.length;
			onAnimEnd();
			transitionningFrom = null;
		} else if (curFrame >= currFrames.length) {
			if (transitionningFrom == null) {
				curFrame = currFrames.length;
				if (curFrame != prev)
					onAnimEnd();
			} else {
				curFrame = 0;
				transitionningFrom = null;
			}
		}
	}

	/**
		Return the tile at current frame.
	**/
	public function getFrame():Tile {
		var i = Std.int(curFrame);
		var currFrames = getCurrSequenceFrames();
		if (i == currFrames.length)
			i--;
		return frames[currFrames[i]];
	}

	public function getCurrSequenceFrames():Array<Int> {
		// return current sequence if there is no transition occuring
		if (transitionningFrom == null)
			return sequences.get(currentSequence).sure().framesIdx;
		// else return transition sequence
		return sequences.get(currentSequence).sure().transitionFrom.sure().get(transitionningFrom.sure()).sure().framesIdx;
	}

	public function getCurrSequenceExpo(curFrame:Int):Float {
		// return current sequence if there is no transiton occuring
		if (transitionningFrom == null)
			return switch (sequences.get(currentSequence).sure().expositions) {
				case Single(v): v;
				case List(v): v[Std.int(curFrame)];
			};
		// else return transition sequence
		return switch (sequences.get(currentSequence).sure().transitionFrom.sure().get(transitionningFrom.sure()).sure().expositions) {
			case Single(v): v;
			case List(v): v[Std.int(curFrame)];
		};
	}

	override function draw(ctx:RenderContext) {
		var t = getFrame();
		if (fading) {
			var currFrames = getCurrSequenceFrames();
			var i = Std.int(curFrame) + 1;
			if (i >= currFrames.length) {
				if (!loop)
					return;
				i = 0;
			}
			var t2 = frames[currFrames[i]];
			var old = ctx.globalAlpha;
			var alpha = curFrame - Std.int(curFrame);
			ctx.globalAlpha *= 1 - alpha;
			emitTile(ctx, t);
			ctx.globalAlpha = old * alpha;
			emitTile(ctx, t2);
			ctx.globalAlpha = old;
		} else {
			emitTile(ctx, t);
		}
	}
}
