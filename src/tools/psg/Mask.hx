
package tools.psg;
import hxd.BitmapData;

class Mask 
{

  public var data:Array<Int>;
  public var width:Int;
  public var height:Int;
  public var mirrorX:Bool;
  public var mirrorY:Bool;
  
  /**
   *   The Mask class defines a 2D template form which sprites can be generated.
   *
   *   @class Mask
   *   @constructor
   *   @param {data} Integer array describing which parts of the sprite should be
   *   empty, body, and border. The mask only defines a semi-ridgid stucture
   *   which might not strictly be followed based on randomly generated numbers.
   *
   *      -1 = Always border (black)
   *       0 = Empty
   *       1 = Randomly chosen Empty/Body
   *       2 = Randomly chosen Border/Body
   *
   *   @param {width} Width of the mask data array
   *   @param {height} Height of the mask data array
   *   @param {mirrorX} A boolean describing whether the mask should be mirrored on the x axis
   *   @param {mirrorY} A boolean describing whether the mask should be mirrored on the y axis
   */
  public function new(data:Array<Int>, width:Int, height:Int, mirrorX:Bool = true, mirrorY:Bool = true):Void
  {
    this.data = data;
    this.width = width;
    this.height = height;
    this.mirrorX = mirrorX;
    this.mirrorY = mirrorY;
  }
  
  /**
   * User an image as a template to generate the Mask.
   * black (0xFF000000) 		=> -1 = Always border (black)
   * transparent (0x00000000)	=>  0 = Empty
   * white (0xFFFFFF)		    => 1 = Randomly chosen Empty/Body
   * any other     			    => 2 = Randomly chosen Border/Body
   * @param	bitmapData
   * @return
   */
  public static function fromBitmapData(bitmapData:BitmapData, ?mirrorX:Bool = true, ?mirrorY:Bool = true):Mask
  {
		var BLACK = 0xFF000000;
		var WHITE = 0xFFFFFFFF;
	  
		var array:Array<Int> = new Array<Int>();
		for (y in 0...bitmapData.height) {
			for (x in 0...bitmapData.width) {
				var color:Int = bitmapData.getPixel(x, y);

				var value = 2;
				if (color == BLACK) {
					value = -1;
				} else if ((color << 24) == 0) { // transparent
					value = 0;
				} else if (color == WHITE) {
					value = 1;
				}
				
				array.push(value);
			}
		}
		
		return new Mask(array, bitmapData.width, bitmapData.height, mirrorX, mirrorY);
  }

}
