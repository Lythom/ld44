package scenes;

using Safety;

import tools.debug.Spy;
import h3d.Engine;
import nape.geom.Vec2;
import nape.phys.Body;
import nape.phys.BodyType;
import nape.phys.Material;
import nape.shape.Circle;
import nape.shape.Polygon;
import nape.space.Space;

/**
 * ...
 * @author Samuel Bouchet
 */
class NapeScene extends h2d.Scene implements IScene {
	var app:Main;

	public static var instance:Null<h2d.Scene>;

	private var space:Space = new Space(new Vec2(0, 100));
	// private var joint:PivotJoint;
	var floorBody:Body = new Body(BodyType.STATIC);
	var circlesBody:Array<Body> = [];
	private var graphics:Null<h2d.Graphics>;

	public function new(app:Main) {
		super();
		this.app = app;
		instance = this;
		initNape();
	}

	override function onAdd() {
		super.onAdd();
		app.engine.backgroundColor = 0x012345;
		this.graphics = new h2d.Graphics(this);
		var spy:Spy = new Spy(this);
		spy.addText("", () -> Std.string(this.i));
	}

	var i = 0;

	override function render(engine:Engine) {
		super.render(engine);
		this.graphics.run(graphics -> {
			graphics.clear();
			graphics.beginFill(0xEA8220);
			floorBody.shapes.foreach(shape -> {
				var p:Null<Polygon> = cast shape;
				p.run(p -> {
					var i = 0;
					@:nullSafety(Off)
					p.localVerts.foreach(vert -> {
						if (i == 0) {
							graphics.moveTo(vert.x, vert.y);
						} else {
							graphics.lineTo(vert.x, vert.y);
						}
						i++;
					});
				});
			});
			graphics.endFill();
			graphics.beginFill(0x82EA20);
			for (body in circlesBody) {
				var circle:Circle = cast body.shapes.at(0);
				graphics.drawCircle(body.position.x, body.position.y, circle.radius);
			}
			graphics.endFill();
		});
	}

	private function initNape() {
		// space
		var gravity:Vec2 = new Vec2(0, 100); // units are pixels/second/second
		// space.worldAngularDrag = 0;
		// space.worldLinearDrag = 0;

		// floor
		var floorShape:Polygon = new Polygon(Polygon.rect(0, this.height - 20, this.width - 10, 10));
		floorShape.body = floorBody;

		floorShape = new Polygon(Polygon.rect(0, 0, 10, this.height - 10));
		floorShape.body = floorBody;

		floorShape = new Polygon(Polygon.rect(this.width - 10, 0, 10, this.height - 10));
		floorShape.body = floorBody;

		floorBody.space = space;

		for (i in 0...5) {
			var circle = new Body(BodyType.DYNAMIC);
			var bouncyRubber = Material.rubber();
			bouncyRubber.elasticity = 1 + i * 0.2;
			circle.shapes.add(new Circle(20 + 5 * i, null, bouncyRubber));
			circle.position.setxy(50 + 50 * i * Math.random(), 50 + 50 * i * Math.random());
			circle.mass = i * 10 + 1;
			circle.inertia = 1;
			circle.space = space;
			circlesBody.push(circle);
		}

		// joint = new PivotJoint(space.world, null, Vec2.weak(0, 0), Vec2.weak(0, 0));
		// joint.space = space;
		// joint.active = false;
		// joint.stiff = false;
	}

	public function update(dt:Float) {
		space.step(dt * 2);
	}
}
