package scenes;

import h2d.Flow;
import h2d.Object;
import h2d.Bitmap;
import h2d.Tile;
import tools.debug.Spy;
import h3d.Engine;
import tools.psg.*;

/**
 * ...
 * @author Samuel Bouchet
 */
class SpriteGenScene extends h2d.Scene implements IScene {
	var app:Main;

	public static var instance:Null<h2d.Scene>;

	private var planet2Mask:Mask = new Mask([
		0,0,0,0,0,0,0,0,-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,0,0,0,0,0,
		0,0,0,0,0,0,-1,-1,2,2,2,2,2,2,2,2,-1,-1,0,0,0,0,0,0,
		0,0,0,0,-1,-1,2,2,2,2,2,2,2,2,2,2,2,2,-1,-1,0,0,0,0,
		0,0,0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,0,0,
		0,0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,0,
		0,0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,0,
		0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,
		0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,
		-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
		-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
		-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
		-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
		-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
		-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
		-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
		-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,
		0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,
		0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,
		0,0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,0,
		0,0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,0,
		0,0,0,-1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,-1,0,0,0,
		0,0,0,0,-1,-1,2,2,2,2,2,2,2,2,2,2,2,2,-1,-1,0,0,0,0,
		0,0,0,0,0,0,-1,-1,2,2,2,2,2,2,2,2,-1,-1,0,0,0,0,0,0,
		0,0,0,0,0,0,0,0,-1,-1,-1,-1,-1,-1,-1,-1,0,0,0,0,0,0,0,0
		], 24, 24, false, false); 

		var robotMask = new Mask([
			0, 0, 0, 0,
			0, 1, 1, 1,
			0, 1, 2, 2,
			0, 0, 1, 2,
			0, 0, 0, 2,
			1, 1, 1, 2,
			0, 1, 1, 2,
			0, 0, 0, 2,
			0, 0, 0, 2,
			0, 1, 2, 2,
			1, 1, 0, 0
	], 4, 11, true, false);

	var  humanoidMask = new Mask([
      0, 0, 0, 0, 0, 0, 1, 1,
      0, 0, 0, 0, 0, 1, 2,-1,
      0, 0, 0, 0, 0, 1, 2,-1,
      0, 0, 0, 0, 0, 0, 2,-1,
      0, 0, 0, 0, 1, 1, 2,-1,
      0, 1, 1, 2, 2, 1, 2,-1,
      0, 0, 0, 1, 0, 1, 1, 2,
      0, 0, 0, 0, 1, 1, 1, 2,
      0, 0, 0, 0, 1, 1, 1, 2,
      0, 0, 0, 0, 1, 1, 0, 0,
      0, 0, 0, 1, 1, 1, 0, 0,
      0, 0, 0, 1, 2, 1, 0, 0,
      0, 0, 0, 1, 2, 1, 0, 0,
      0, 0, 0, 1, 2, 2, 0, 0
    ], 8, 14, true, false);
	
	var dragonMask = new Mask([
      0,0,0,0,0,0,0,0,0,0,0,0,
      0,0,0,0,1,1,1,1,0,0,0,0,
      0,0,0,1,1,2,2,1,1,0,0,0,
      0,0,1,1,1,2,2,1,1,1,0,0,
      0,0,0,0,1,1,1,1,1,1,1,0,
      0,0,0,0,0,0,1,1,1,1,1,0,
      0,0,0,0,0,0,1,1,1,1,1,0,
      0,0,0,0,1,1,1,1,1,1,1,0,
      0,0,1,1,1,1,1,1,1,1,0,0,
      0,0,0,1,1,1,1,1,1,0,0,0,
      0,0,0,0,1,1,1,1,0,0,0,0,
      0,0,0,0,0,0,0,0,0,0,0,0
    ], 12, 12, false, false); 

	public function new(app:Main) {
		super();
		this.app = app;
		instance = this;
	}

	override function onAdd() {
		super.onAdd();
		app.engine.backgroundColor = 0x234567;
		var flow:Flow = new Flow(this);
		flow.padding = 10;

		var robot = new Object(flow);
		var planet = new Object(flow);
		var humanoid = new Object(flow);
		var dragon = new Object(flow);

		regenerateSprite(planet, planet2Mask);
		regenerateSprite(robot, robotMask);
		regenerateSprite(humanoid, humanoidMask);
		regenerateSprite(dragon, dragonMask);
	}

	function regenerateSprite(parent:h2d.Object, mask:Mask) 
	{
		var newsprite:Sprite = new Sprite(mask, true, 0.45, 0.4, 0.2, 0.3);
		
		var classicScale:Bitmap = new Bitmap(newsprite.bitmap);
		classicScale.scale(4);
		
		parent.removeChildren();
		parent.addChild(classicScale);
	}


	override function render(engine:Engine) {
		super.render(engine);
	}

	public function update(dt:Float) {}
}
