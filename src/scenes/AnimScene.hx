package scenes;

using Safety;

import tools.debug.Spy;
import platformer.ControlledHero;
import haxe.ds.StringMap;
import hxd.Res;
import h2d.Scene;
import hxd.Key;
import tools.anim.AnimatedSprite;

@:enum
abstract Anims(String) from String to String {
	var Iddle = 'Iddle';
	var Run = 'Run';
	var Jump = 'Jump';
	var Swap = 'Swap';
}

/**
 * @author Samuel Bouchet
 */
class AnimScene extends Scene implements IScene {
	public static var instance:Null<Scene>;

	var app:Main;
	var anim:Null<AnimatedSprite>;

	override function onAdd() {
		super.onAdd();

		// live reload
		hxd.Res.data.watch(() -> {
			Data.load(hxd.Res.data.entry.getText());
			fromData();
		});
		fromData();
		app.engine.backgroundColor = 0xD0CDEC;
	}

	/**
	 * Create initial position from data change
	 */
	public function fromData() {
		this.removeChildren();
		// Yay load from Data !
		// get Data
		var animData:Null<Data.Anims> = Data.anims.get(Data.AnimsKind.Po);
		// load tiles from file reference
		var tiles = hxd.Res.load(animData.sure().spritesheet).toTile().split(animData.tileCount); // // hxd.Res.Test_sprite00000_11x1.toTile().split(11);
		// parse sequences
		var sequences:StringMap<Sequence> = ControlledHero.parseData(animData);

		// var sequences:StringMap<Sequence> = [
		// 	Anims.Iddle => {
		// 		framesIdx: [7],
		// 		expositions: Single(1),
		// 		transitionFrom: [Anims.Run => {
		// 			framesIdx: [9, 10],
		// 			expositions: List([2, 1]),
		// 		},
		// 		Anims.Jump => {
		// 			framesIdx: [8],
		// 			expositions: Single(1),
		// 		}]
		// 	},
		// 	Anims.Run => {
		// 		framesIdx: [0, 1, 2, 3, 4, 5, 6],
		// 		expositions: Single(1),
		// 		transitionFrom: [Anims.Iddle => {
		// 			framesIdx: [10, 8],
		// 			expositions: Single(2),
		// 		}]
		// 	},
		// 	Anims.Jump => {
		// 		framesIdx: [2],
		// 		expositions: Single(2),
		// 		transitionFrom: [
		// 			Anims.Iddle => {
		// 				framesIdx: [8],
		// 				expositions: Single(2),
		// 			},
		// 			Anims.Run => {
		// 				framesIdx: [8],
		// 				expositions: Single(2),
		// 			}
		// 		]
		// 	},
		// 	Anims.Swap => {
		// 		framesIdx: [9],
		// 		expositions: Single(1),
		// 	}
		// ];
		var ani = new AnimatedSprite(tiles, sequences, 12, this);
		anim = ani;

		var spy:Spy = new Spy(this);
		spy.addText(Std.string(Std.int(ani.currentFrame)),() -> Std.string(Std.int(ani.currentFrame)));
		for (seqName in sequences.keys()) {
			spy.addButton(seqName,() -> ani.play(seqName));
		}
	}

	public function new(app:Main) {
		super();
		this.app = app;
		instance = this;
	}

	public function update(dt:Float) {}
}
