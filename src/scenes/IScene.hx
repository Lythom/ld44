package scenes;

interface IScene {
	public function update(dt:Float):Void;
}
