package scenes;

using Safety;

import h2d.filter.Blur;
import h2d.filter.Glow;
import h3d.mat.Texture;
import h2d.col.Bounds;
import h2d.TileGroup;
import h2d.CdbLevel;
import h2d.Tile;
import h2d.Bitmap;
import h2d.Object;
import platformer.ControlledHero;
import platformer.RotatingSword;
import platformer.BasicPhysicsObject;
import h2d.Scene;
import tools.Camera;

/**
 * ...
 * @author Samuel Bouchet
 */
class PlatformerScene extends Scene implements IScene {
	public static var instance:Null<Scene>;
	public static var SCALE_RATIO:Float = 4;

	var camera:Null<Camera>;
	var hero:Null<BasicPhysicsObject>;
	var sword:Null<RotatingSword>;
	var app:Main;
	var cdb:Null<CdbLevel>;
	var collisions:Null<Array<Bool>>;
	var bmpDebug:Null<Bitmap>;
	var boundPool = new Bounds();

	override function onAdd() {
		super.onAdd();
		camera = new Camera(this);
		hero = new ControlledHero(0, 0, camera);
		hero.sure().filter = new Blur();
		sword = new RotatingSword(hero.sure(), this);

		// live reload
		// hxd.Res.data.watch(() -> {
		// 	Data.load(hxd.Res.data.entry.getText());
		// 	fromData();
		// });
		fromData();
		app.engine.backgroundColor = 0xFEDCBA;
	}

	/**
	 * Create initial position from data change
	 */
	public function fromData() {
		var layer = new TileGroup(hxd.Res.chat.toTile(), camera);

		// Automatic display of tiles
		var cdb = new h2d.CdbLevel(Data.levels, 0, camera);
		cdb.scale(SCALE_RATIO);
		cdb.redraw();
		this.cdb = cdb;
		var level = Data.levels.all[0];

		// collision matrice for the level
		this.collisions = cdb.getLevelLayer("Test").buildIntProperty("collides").map(i -> i == 1);

		// manual display of custom tiles
		var mapTiles = level.blocks.decode(Data.tiles.all);
		var visualTiles = hxd.Res.forest_12x12.toTile();

		layer.clear();
		for (y in 0...level.height)
			for (x in 0...level.width) {
				var t = mapTiles[x + y * level.width];
				if (t != null && (t.tile.x != 0 || t.tile.y != 0)) {
					var size = t.tile.size;
					var tile:Tile = visualTiles.sub(t.tile.x * size, t.tile.y * size, size, size);
					if (t.collides) {
						tile.setTexture(Texture.fromColor(0xFF0000, 0.5).sure());
					}
					layer.add(x * size, y * size, tile);
				}
			}
		layer.scale(SCALE_RATIO);

		var h = Data.heros.all[0];
		hero.run(hero -> {
			hero.x = h.x;
			hero.y = h.y;
			over(hero);
		});

		sword.run(sword -> {
			var d = Data.swords.all[0];
			sword.distance = d.distance;
			sword.scaleX = d.size / 12;
			sword.scaleY = d.size / 12;
			over(sword);
		});
	}

	public function groundCollides(collisionMap:Array<Bool>, levelWidth:Int, tileSize:Float, b:Bounds):Bool {
		if (collisionMap == null || b.xMax < 0 || b.xMin > levelWidth * tileSize)
			return false;

		// all tiles in bounds
		b.xMin = Math.round(b.xMin) / tileSize;
		b.xMax = Math.round(b.xMax) / tileSize;
		b.yMin = Math.round(b.yMin) / tileSize;
		b.yMax = Math.round(b.yMax) / tileSize;
		var tileIndexes = [
			for (tx in Math.floor(b.xMin)...Math.ceil(b.xMax))
				for (ty in Math.floor(b.yMin)...Math.ceil(b.yMax))
					tx + ty * levelWidth];

		// check collidability of each tile
		for (i in tileIndexes) {
			if (i < 0 || i > collisionMap.length)
				break;
			if (collisionMap[i])
				return true;
		}
		return false;
	}

	function collideWithGround(object:Object, x:Float, y:Float) {
		if (!Std.is(object, ControlledHero))
			return null;
		var hero:ControlledHero = cast(object, ControlledHero);
		boundPool.load(hero.hitbox);
		boundPool.offset(x, y);
		boundPool.offset(hero.absX - camera.sure().x, hero.absY - camera.sure().y);
		var collides = groundCollides(this.collisions.sure(), cdb.sure().level.width, cdb.sure().level.props.tileSize.sure() * SCALE_RATIO, boundPool);
		return collides ? cdb : null;
	}

	public function new(app:Main) {
		super();

		this.app = app;
		instance = this;
	}

	public function update(dt:Float) {
		hero.run(hero -> {
			hero.update(dt, collideWithGround);
			camera.run(camera -> {
				camera.viewX = hero.x;
				camera.viewY = hero.y;
			});
		});
		sword.run(sword -> {
			sword.update(dt);
		});
	}
}
