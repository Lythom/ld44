package scenes;

import h2d.Object;
import h2d.Flow.FlowAlign;
import h2d.HtmlText;
import lythom.stuffme.ItemDetail;
import lythom.stuffme.Bonus;
import lythom.stuffme.Item;
import lythom.stuffme.AttributeValues;
import hxd.res.DefaultFont;
import hxd.Res;
import h2d.Scene;

using lythom.stuffme.StuffMe;
using Lambda;
using Safety;

@:enum
abstract Attr(String) from String to String {
	@:from
	public static function fromData(d:Data.Items_bonus_prop):Attr {
		return switch (d) {
			case Data.Items_bonus_prop.ManaMax: ManaMax;
			case Data.Items_bonus_prop.Strength: Strength;
			case Data.Items_bonus_prop.AbilityPower: AbilityPower;
			case Data.Items_bonus_prop.Defense: Defense;
			case Data.Items_bonus_prop.HPMax: HPMax;
		}
	}

	var Strength = "Strength";
	var ManaMax = "ManaMax";
	var AbilityPower = "AbilityPower";
	var Defense = "Defense";
	var HPMax = "HPMax";
}

/**
 * @author Samuel Bouchet
 */
class AttributeTestScene extends Scene implements IScene {
	public static var instance:Null<Scene>;

	var app:Main;
	var fui:h2d.Flow = new h2d.Flow();
	var heroStats:AttributeValues = new AttributeValues();

	override function onAdd() {
		super.onAdd();
		fui = new h2d.Flow(this);
		// fui.isVertical = true;
		fui.verticalSpacing = 10;
		fui.horizontalSpacing = 20;
		fui.padding = 10;
		fui.maxWidth = this.width;
		// fui.overflow = true;
		fui.verticalAlign = FlowAlign.Top;
		fui.multiline = true;
		// fui.colWidth = this.width;

		heroStats.set(Attr.Strength, 10);
		heroStats.set(Attr.ManaMax, 100);
		heroStats.set(Attr.AbilityPower, 10);

		// live reload
		hxd.Res.data.watch(() -> {
			Data.load(hxd.Res.data.entry.getText());
			fromData();
		});
		fromData();
	}

	/**
	 * Create initial position from data change
	 */
	public function fromData() {
		fui.removeChildren();

		addBlock('Hero Stats:', t -> displayStats(t, heroStats), fui);

		// HR
		new h2d.Bitmap(h2d.Tile.fromColor(0xCCCCCC, this.width, 1), fui);

		// create items from Data
		var items = [
			for (i in Data.items.all)
				i.id => new Item(i.name, [
					for (b in i.bonus.sure())
						new Bonus(getFormula(b.operation, Attr.fromData(b.prop)), getDescription(b.desc, b.operation, b.prop), b.priority.toInt())])];

		// display stats with each item
		for (item in items) {
			addBlock('With <font color="#DDDD33">${item.id}</font>:', t -> displayCalculatedStuff(t, heroStats.with([item])), fui);
		}

		// HR
		new h2d.Bitmap(h2d.Tile.fromColor(0xCCCCCC, this.width, 1), fui);

		// particular situations
		var archangel = items.get(Data.ItemsKind.archangel).sure();
		var rabadon = items.get(Data.ItemsKind.rabadon).sure();
		addBlock('With <font color="#DDDD33">Rabadon + Archangel Staff</font>:', t -> displayCalculatedStuff(t, heroStats.with([archangel, rabadon])), fui);

		var shield = items.get(Data.ItemsKind.basic_shield).sure();
		var bsword = items.get(Data.ItemsKind.special_sword).sure();
		addBlock('With <font color="#DDDD33">Special Sword, Basic Shield</font>', t -> displayCalculatedStuff(t, heroStats.with([bsword, shield])), fui);

		var bshield = items.get(Data.ItemsKind.better_shield).sure();
		addBlock('With <font color="#DDDD33">Special Sword, Better Shield</font>', t -> displayCalculatedStuff(t, heroStats.with([bsword, bshield])), fui);

		var defGemMod = items.get(Data.ItemsKind.def_gem_mod).sure();
		var defGem = items.get(Data.ItemsKind.def_gem).sure();
		var sword = items.get(Data.ItemsKind.basic_sword).sure();

		shield.equipedItems.push(defGem);
		defGem.equipedItems.push(defGemMod);
		addBlock('With <font color="#DDDD33">Sword, Shield, Shield gem, and Shield gem modifier</font>',
			t -> displayCalculatedStuff(t, heroStats.with([sword, shield])), fui);
	}

	function getFormula(op:Data.Operation, prop:Attr):Formula {
		switch (op) {
			case Add(v):
				return args -> [prop => v];
			case Mult(v):
				return args -> [prop => args.values.get(prop).or(0) * v];
			case Custom(functionName):
				if (functionName == 'archangelStaffManaToAP') {
					return args -> [Attr.AbilityPower => args.values.get(Attr.ManaMax).or(0) * 0.030];
				}
				if (functionName == 'SpecialSwordDef') {
					return args -> {
						if (args.siblings.exists(i -> i.id == "Better Shield")) {
							return [Attr.Strength => args.values.get(Attr.Strength).or(0) * 0.5];
						}
						return new AttributeValues();
					}
				}
		}
		return args -> new AttributeValues();
	}

	function getDescription(description:Null<String>, op:Data.Operation, prop:Attr):DynamicDescription {
		if (description != null)
			return args -> description.or('');
		return switch (op) {
			case Add(v): argv -> 'Add +$v to $prop';
			case Mult(v): argv -> 'Add +${v * 100}% of $prop';
			case Custom(functionName): argv -> '';
		}
	}

	function addBlock(title:String, displayContent:StringBuf->Void, parent:h2d.Object) {
		var t = new StringBuf();
		t.add('$title<br/>');
		displayContent(t);
		var h = new HtmlText(DefaultFont.get(), parent);
		h.text = t.toString();
	}

	function displayStats(t:StringBuf, stats:AttributeValues) {
		for (attr => value in stats.keyValueIterator()) {
			t.add('  $attr: <font color="#DDAA33">$value</font><br/>');
		}
	}

	function displayCalculatedStuff(t:StringBuf, stuff:CalculatedStuff) {
		t.add('  Final Values<br/>');
		for (attr => value in stuff.values.keyValueIterator()) {
			var baseValue = heroStats.get(attr).or(0);
			var color = value == baseValue ? '#DDAA33' : '#33CC33';
			var diff = value - baseValue;
			var plus = diff == 0 ? '' : diff > 0 ? '(+$diff)' : '($diff)';
			t.add('  $attr: <font color="$color">$value</font> $plus<br/>');
		}
		t.add(' _Breakdown:<br/>');
		for (itemDetail in stuff.items) {
			displayItemDetail(t, itemDetail);
		}
	}

	function displayItemDetail(t:StringBuf, detail:ItemDetail, padding:String = "") {
		t.add('$padding __SubItem ${detail.item.id}:<br/>');
		for (b in detail.bonuses) {
			for (attr => value in b.value.keyValueIterator()) {
				t.add('$padding        +$value $attr (${b.description})<br/>');
			}
		}
		for (detail in detail.items) {
			displayItemDetail(t, detail, padding + "__");
		}
	}

	public function new(app:Main) {
		super();
		this.app = app;
		instance = this;
	}

	public function update(dt:Float) {
		if (this.fui != null) {
			var scrollHeight = this.fui.outerHeight - this.height;
			var percent = hxd.Math.clamp(mouseY / this.height, 0, 1);
			this.fui.y = Math.round(-scrollHeight * percent);
		}
	}
}
