package platformer;

using Safety;

import haxe.Json;
import scenes.AnimScene.Anims;
import haxe.ds.StringMap;
import tools.anim.AnimatedSprite;
import h2d.col.Bounds;
import platformer.BasicPhysicsObject.DetectCollision;
import h2d.Object;
import h2d.col.Point;
import h2d.Bitmap;
import hxd.Key;

/**
 * ...
 * @author Lythom
 */
class ControlledHero extends BasicPhysicsObject {
	public var hitbox(default, null):Bounds = Bounds.fromValues(0, 0, 0, 0);
	public var anim:Null<AnimatedSprite>;

	public static var SCALE:Float = 4;

	var gravity:Point;
	var jumping:Bool = false;

	public function new(x:Float = 0, y:Float = 0, ?parent:Object) {
		super(x, y, parent);
		// setHitbox(bmpPerso.width - 1, bmpPerso.height - 1);

		// 20 px = 1m / second
		gravity = new Point(0, (9.81 * 20 * SCALE));
		this.forces.push(gravity);
	}

	override function onAdd() {
		super.onAdd();

		// live reload
		// hxd.Res.data.watch(() -> {
		// 	Data.load(hxd.Res.data.entry.getText());
		// 	fromData();
		// });
		fromData();
	}

	public static function parseSequence(seq:{
		expositions:String,
		framesIdx:String
	}):Sequence {
		var expo = Json.parse(seq.expositions);
		return ({
			framesIdx: Json.parse(seq.framesIdx),
			expositions: (Std.is(expo, Float) ? Single(cast(expo, Float)) : List(cast expo))
		});
	}

	public static function parseData(anim:Data.Anims):StringMap<Sequence> {
		var result = new StringMap<Sequence>();
		for (seqData in anim.sequences) {
			var seq = parseSequence({expositions: seqData.expositions, framesIdx: seqData.framesIdx});
			var transitionFrom:Null<StringMap<Sequence>> = null;
			if (seqData.transitionFrom.length > 0) {
				transitionFrom = new StringMap<Sequence>();
				for (tf in seqData.transitionFrom) {
					transitionFrom.set(tf.fromSequence, parseSequence({expositions: tf.expositions, framesIdx: tf.framesIdx}));
				}
			}
			seq.transitionFrom = transitionFrom;
			result.set(seqData.name.toString(), seq);
		}
		return result;
	}

	public function fromData() {
		this.removeChildren();

		// Yay load from Data !
		// get Data
		var animData:Data.Anims = Data.anims.get(Data.AnimsKind.Po).sure();
		// load tiles from file reference
		var tiles = hxd.Res.load(animData.spritesheet).toTile().split(animData.tileCount); // // hxd.Res.Test_sprite00000_11x1.toTile().split(11);
		// parse sequences
		var sequences:StringMap<Sequence> = parseData(animData);
		for (tile in tiles) {
			tile.setCenterRatio();
		}

		var bmpPerso:Bitmap = new Bitmap(h2d.Tile.fromColor(0xFFFFFF, tiles[0].width, tiles[0].height), this);
		bmpPerso.color = new h3d.Vector(1, 1, 0.2, 0.3);
		bmpPerso.tile.setCenterRatio();

		var anim = new AnimatedSprite(tiles, sequences, 12, this);
		this.hitbox = Bounds.fromValues(tiles[0].dx, tiles[0].dy, tiles[0].width, tiles[0].height);
		anim.play(Anims.Run);
		this.anim = anim;
	}

	override public function update(dt:Float, collide:DetectCollision, forceFriction:Bool = false):Void {
		this.acceleration.normalize();
		var xAcc:Float = 1000 * SCALE;
		var yAcc:Float = 1000 * SCALE;
		var facteur:Float = 1;

		if (Key.isDown(Key.SHIFT)) {
			facteur = 2;
		}
		maxVelocity.x = 150 * facteur * SCALE;

		if (onGround) {
			jumping = false;
		}
		if (Key.isDown(Key.UP) && !jumping) {
			this.acceleration.y = -150 * SCALE / dt;
			jumping = true;
		}

		if (Key.isDown(Key.LEFT)) {
			this.acceleration.x = -xAcc * facteur;
			friction.x = 0;
			anim.run(anim -> {
				if (anim.scaleX > 0)
					anim.scaleX = anim.scaleX * -1;
			});
		} else if (Key.isDown(Key.RIGHT)) {
			this.acceleration.x = xAcc * facteur;
			friction.x = 0;
			anim.run(anim -> {
				if (anim.scaleX < 0)
					anim.scaleX = anim.scaleX * -1;
			});
		} else {
			this.acceleration.x = 0;
			friction.x = 0.2;
		}

		anim.run(anim -> {
			if (jumping) {
				anim.play(Anims.Jump);
			} else if (!Key.isDown(Key.LEFT) && !Key.isDown(Key.RIGHT)) {
				anim.play(Anims.Iddle);
			} else if ((Key.isDown(Key.LEFT) && velocity.x > 0) || (Key.isDown(Key.RIGHT) && velocity.x < 0)) {
				anim.play(Anims.Swap);
			} else {
				anim.play(Anims.Run);
			}
		});

		if (Key.isPressed(Key.NUMPAD_ADD)) {
			gravity.y++;
			trace(gravity.y);
		}

		if (Key.isPressed(Key.NUMPAD_SUB)) {
			gravity.y--;
			trace(gravity.y);
		}

		if (this.y > 480 * SCALE) {
			this.x = 100;
			this.y = 0;
		}

		//   else if (!Key.isDown(Key.RIGHT) && !Key.isDown(Key.LEFT)) {
		//     AnimatedSprite.transitionToAnimation(entity, 'iddle')
		//   } else if (Key.isDown(Key.RIGHT) && entity.velocityX < 0) {
		//     AnimatedSprite.transitionToAnimation(entity, 'swap')
		//     entity.flipX = true
		//   } else if (Key.isDown(Key.LEFT) && entity.velocityX > 0) {
		//     AnimatedSprite.transitionToAnimation(entity, 'swap')
		//     entity.flipX = false
		//   } else {
		//     AnimatedSprite.transitionToAnimation(entity, 'run')
		//   }

		super.update(dt, collide);
	}
}
