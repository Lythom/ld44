package platformer;

using Safety;

import h2d.Object;
import h2d.col.Point;

/**
 * @param Object Object to check collision with
 * @param x x position offset
 * @param y y position offset
 * @return The object collided at designated position, null if no collision
 */
typedef DetectCollision = Object->Float->Float->Null<Object>;

/**
 * Object moving using forces and manual acceleration.
 */
class BasicPhysicsObject extends Object {
	/** Current per update moving speed of the Object. */
	public var velocity:Point = new Point();

	/** Current per update speed acceleration of the Object. */
	public var acceleration:Point = new Point();

	/** max speed of the Object. */
	public var maxVelocity:Point = new Point();

	/** Direction the Object is facing as a 2D vector (Point) */
	public var orientation:Point = new Point();

	/** Direction the Object is facing in Radian */
	public var orientationInRadian(get, null):Float = 0;

	function get_orientationInRadian():Float {
		return Math.atan2(orientation.y, orientation.x);
	}

	/** List of forces applied to Object. Gravity should be added here. */
	public var forces:Array<Point> = new Array<Point>();
	public var onGround(default, null):Bool = false;
	
	public var onWall(default, null):Bool = false;
	public var friction:Point = new Point();

	// function preallocated resources
	var normalizedVelocity:Point = new Point();

	/** Collision type to evaluate. */
	public var solid:String = "solid";

	public function new(x:Float, y:Float, ?parent:Object) {
		super(parent);

		this.x = x;
		this.y = y;

		velocity = new Point(0, 0);
		acceleration = new Point(0, 0);
		friction = new Point(0.2, 0);
		maxVelocity = new Point(200, 1000);
		orientation = new Point(1, 0);
		forces = new Array<Point>();

		normalizedVelocity = new Point(0, 0);
	}

	public function update(dt:Float, collide:DetectCollision, forceFriction:Bool = false):Void {
		// apply acceleration if maxVelocity is not reach
		applyAcceleration(dt);
		applyForces(dt);
		var res = applyVelocityAndDetectCollisions(dt, collide);
		this.onGround = res.onGround;
		this.onWall = res.onWall;
		velocity.x = applyFriction(forceFriction || onGround, velocity.x, friction.x);
		velocity.y = applyFriction(forceFriction || onWall, velocity.y, friction.y);
		this.x += velocity.x * dt;
		this.y += velocity.y * dt;
	}

	/** Change velocity according to acceleration if maxVelocity is not reached. */
	private function applyAcceleration(dt:Float):Void {
		// accelerate while velocity < maxVelocity
		if (Math.abs(velocity.x + acceleration.x * dt) <= maxVelocity.x || maxVelocity.x <= 0) {
			velocity.x += acceleration.x * dt;
			// cap to maxVelocity
		} else if (Math.abs(velocity.x) > maxVelocity.x) {
			velocity.x = maxVelocity.x * sign(velocity.x);
		}

		// accelarate while velocity < maxVelocity
		if (Math.abs(velocity.y + acceleration.y * dt) <= maxVelocity.y || maxVelocity.y <= 0) {
			velocity.y += acceleration.y * dt;
			// cap to maxVelocity
		} else if (Math.abs(velocity.y) > maxVelocity.y) {
			velocity.y = maxVelocity.y * sign(velocity.y);
		}
	}

	/** Change velocity according to forces applied on the Object. */
	public function applyForces(dt:Float):Void {
		for (f in forces) {
			velocity.x += f.x * dt;
			velocity.y += f.y * dt;
		}
	}

	/**
	 * Change velocity according to friction
	 */
	public function applyFriction(attached:Bool, vel:Float, fric:Float):Float {
		if (attached && fric != 0.0) {
			var amount = (vel * fric);
			vel = vel - amount;
			if (Math.abs(vel) < 0.9) {
				vel = 0;
			}
		}
		return vel;
	}

	/** apply velocity to coordinates when possible, stop with collisions.  */
	public function applyVelocityAndDetectCollisions(dt:Float, collide:DetectCollision):{onGround:Bool,onWall:Bool} {
		var onGround = false;
		var onWall = false;
		var moveY:Bool = false;
		var moveX:Bool = false;

		// check Y collisions
		collide(this, 0, velocity.y * dt).run(collided -> {
			// place at nearest position before collision
			var moveY:Float = velocity.y * dt;
			if (Math.abs(velocity.y) >= 1) {
				while (collide(this, 0, moveY) != null) {
					moveY -= sign(velocity.y) * 0.2;
				}
			}
			y = y + moveY;
			onGround = true;
			velocity.y = 0;
		});

		// check X collisions
		collide(this, velocity.x * dt, 0).run(collided -> {
			// place at nearest position before collision
			var moveX:Float = velocity.x * dt;
			if (Math.abs(velocity.x) >= 1) {
				while (collide(this, moveX, 0) != null) {
					moveX -= sign(velocity.x) * 0.2;
				}
			}
			x = x + moveX;
			onWall = true;
			velocity.x = 0;
		});

		return {onGround: onGround, onWall: onWall};
	}

	@:inline
	function sign(number:Float):Float {
		if (number == 0)
			return 0;
		return number > 0 ? 1 : -1;
	}
}
