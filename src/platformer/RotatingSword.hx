package platformer;

import hxd.Key;
import h2d.Bitmap;
import h2d.Tile;
import h2d.Object;

/**
 * ...
 * @author Samuel Bouchet
 */
class RotatingSword extends Object {
	/**
	 * In number of turn per second
	 */
	public var rotationSpeed:Float = 0;
	public var distance:Float = 0;

	private var bitmap:Bitmap;
	private var _hero:Object;

	public function new(hero:Object, parent:Object) {
		super(parent);
		rotationSpeed = -2;
		distance = 5;
		_hero = hero;
		
		var tile:Tile = Tile.fromColor(0xcc2255, 8, 48);
		bitmap = new Bitmap(tile, this);
		bitmap.tile.dy = -624;
		bitmap.tile.dx = -4;
	}

	public function update(dt:Float):Void {
		if (Key.isReleased(Key.MOUSE_LEFT)) {
			rotationSpeed = -rotationSpeed;
		}

		if (Key.isPressed(Key.NUMPAD_0)) {
			rotationSpeed = 0;
		}

		if (Key.isPressed(Key.NUMPAD_1)) {
			rotationSpeed = 0.5;
		}

		if (Key.isPressed(Key.NUMPAD_2)) {
			rotationSpeed = 1;
		}
		updatePosition(dt);
	}

	public function updatePosition(dt:Float):Void {
		this.bitmap.rotate(rotationSpeed * dt * Math.PI);
		bitmap.tile.dy = Math.round(this.bitmap.tile.height / 2 + distance);
		this.absX = _hero.absX + distance * Math.cos((this.bitmap.rotation * Math.PI * 2) / 360);
		this.absY = _hero.absY - distance * Math.sin((this.bitmap.rotation * Math.PI * 2) / 360);
	}
}
