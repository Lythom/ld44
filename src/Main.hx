import bacon.MainScene;
import tweenx909.advanced.UpdateModeX;
import tweenx909.TweenX;
import h2d.Flow.FlowAlign;
import hxd.Key;
import hxd.SceneEvents.InteractiveScene;
import scenes.IScene;

// import scenes.AnimScene;
// import scenes.PlatformerScene;
// import scenes.AttributeTestScene;
// import scenes.NapeScene;
// import scenes.SpriteGenScene;
using Safety;

class Main extends hxd.App {
	public static var tileWidth = 12;
	public static var tileHeight = 12;
	public static var appWidth = tileWidth * 96;
	public static var appHeight = tileHeight * 64;
	public static var scale:Int = 2;

	var fui:Null<h2d.Flow>;

	public var menuScene:Null<InteractiveScene>;

	public function new() {
		super();
	}

	function onReload() {
		var data = hxd.Res.data.entry.getText();
		Data.load(data);
	}

	override public function setScene(scene:InteractiveScene, disposePrevious:Bool = true) {
		super.setScene(scene, disposePrevious);
		engine.backgroundColor = 0x303030;
		// engine.width = appWidth;
		// engine.height = appWidth;
	}

	override function init() {
		hxd.Res.data.watch(onReload);

		var window = hxd.Window.getInstance();
		scale = Std.int(Math.min(hxd.System.width / appWidth, hxd.System.height / appHeight));
		window.resize(appWidth * scale, appHeight * scale);

		TweenX.updateMode = UpdateModeX.MANUAL;
		menuScene = this.s2d;

		bacon.AnimationData.fromData();

		play(MainScene)();

		// var fui = new h2d.Flow(s2d);
		// this.fui = fui;
		// fui.layout = Vertical;
		// fui.verticalSpacing = 5;
		// fui.padding = 10;
		// fui.horizontalAlign = FlowAlign.Middle;
		// fui.scale(2);

		// addButton(fui, "1. Play Action platformer", play(PlatformerScene));
		// addButton(fui, "2. Attributes Test", play(AttributeTestScene));
		// addButton(fui, "3. Animation", play(AnimScene));
		// addButton(fui, "4. Physics", play(NapeScene));
		// addButton(fui, "5. SpriteGen", play(SpriteGenScene));

		// entries.set("2. Attributes", AttributesTestWorld);
		// //entries.set("3. Animation", AnimationTestWorld);
		// entries.set("3. BoardGame", BoardGameWorld);
		// entries.set("4. Translation", TranslationTestWorld);
		// entries.set("5. Normal Mapping", NormalMappingTestWorld);
		// //entries.set("6. Physics engine (Nape)", NapeTestWorld);
		// entries.set("6. Sprite Gen", SpriteGenScene);
		// entries.set("7. Test", TestWorld);
		// entries.set("8. RPG", RpgWorld);
	}

	function play(scene:Class<h2d.Scene>):Void->Void {
		var scene = Type.createInstance(scene, [this]);
		scene.setFixedSize(appWidth, appHeight);
		return() -> setScene2D(scene);
	}

	override function update(dt:Float) {
		TweenX.manualUpdate(dt);
		super.update(dt);

		fui.run(fui -> {
			if (fui.visible) {
				fui.x = Math.round(appWidth / 2 - fui.outerWidth);
				fui.y = Math.round(appHeight / 3 - fui.outerHeight);
			}
		});

		if (Std.is(s2d, IScene)) {
			cast(s2d, IScene).update(dt);
		}
		if (Key.isPressed(Key.NUMPAD_1)) {
			play(MainScene)();
		}
		// if (Key.isPressed(Key.NUMPAD_2)) {
		// 	play(AttributeTestScene)();
		// }
		// if (Key.isPressed(Key.NUMPAD_3)) {
		// 	play(AnimScene)();
		// }
		// if (Key.isPressed(Key.NUMPAD_4)) {
		// 	play(NapeScene)();
		// }
		// if (Key.isPressed(Key.NUMPAD_5)) {
		// 	play(SpriteGenScene)();
		// }
		menuScene.run(menuScene -> {
			if (Key.isPressed(Key.ESCAPE)) {
				setScene(menuScene);
			}
		});
	}

	static function main() {
		#if js
		hxd.Res.initEmbed({compressSounds: true});
		#else
		hxd.res.Resource.LIVE_UPDATE = true;
		hxd.Res.initLocal();
		#end

		var data = hxd.Res.data.entry.getText();
		Data.load(data);

		new Main();
	}

	function addButton(parent:h2d.Flow, label:String, onClick:Void->Void) {
		var f = new h2d.Flow(parent);
		f.padding = 5;
		f.paddingBottom = 7;
		f.backgroundTile = h2d.Tile.fromColor(0x404040);
		var tf = new h2d.Text(getFont(), f);
		tf.text = label;
		f.enableInteractive = true;
		f.interactive.cursor = Button;
		f.interactive.onClick = function(_) onClick();
		f.interactive.onOver = function(_) f.backgroundTile = h2d.Tile.fromColor(0x606060);
		f.interactive.onOut = function(_) f.backgroundTile = h2d.Tile.fromColor(0x404040);
		return f;
	}

	function getFont() {
		return hxd.res.DefaultFont.get();
	}
}
