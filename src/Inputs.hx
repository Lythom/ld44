import hxd.Key;

class Inputs {

    public static var isSpelling:Bool = false;

    public static function MoveUp():Bool {
        return !isSpelling && (Key.isDown(Key.UP) || Key.isDown(Key.Z) || Key.isDown(Key.W));
    }
    public static function MoveDown():Bool {
        return !isSpelling && (Key.isDown(Key.DOWN) || Key.isDown(Key.S));
    }
    public static function MoveLeft():Bool {
        return !isSpelling && (Key.isDown(Key.LEFT) || Key.isDown(Key.A) || Key.isDown(Key.Q));
    }
    public static function MoveRight():Bool {
        return !isSpelling && (Key.isDown(Key.RIGHT) || Key.isDown(Key.D));
    }
    public static function SpellTyping():Bool {
        return Key.isPressed(Key.ENTER);
    }
}