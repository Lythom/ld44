import utest.Assert;
import bacon.spelling.Spell;

using buddy.Should;
using Safety;

class Tests extends buddy.SingleSuite {
	public function new() {
		describe("spread Spells", {
			it("should spread equally among 1 columns", {
				var baseAngle = Math.PI;
				var sequence = Spell.schedule(ApplicationKind.Spread(1, 1, 3, SpreadKind.Wave(1)), MaterialKind.Ink, baseAngle, 0).sure();
				Assert.same({
					time: 0,
					angle: baseAngle
				}, sequence.pop().sure());
				Assert.same({
					time: 1,
					angle: baseAngle
				}, sequence.pop().sure());
				Assert.same({
					time: 2,
					angle: baseAngle
				}, sequence.pop().sure());
				sequence.pop().should.be(null);
			});
			it("should spread equally among 2 columns", {
				var baseAngle = Math.PI;
				var sequence = Spell.schedule(ApplicationKind.Spread(1, 0.5, 4, SpreadKind.Wave(2)), MaterialKind.Ink, baseAngle, 0).sure();
				var proj;
				proj = sequence.pop().sure();
				Assert.same({
					time: 0,
					angle: baseAngle - 1 / 2
				}, proj);

				proj = sequence.pop().sure();
				Assert.same({
					time: 2,
					angle: baseAngle + 1 / 2
				}, proj);

				proj = sequence.pop().sure();
				Assert.same({
					time: 4,
					angle: baseAngle - 1 / 2
				}, proj);

				proj = sequence.pop().sure();
				Assert.same({
					time: 6,
					angle: baseAngle + 1 / 2
				}, proj);
				sequence.pop().should.be(null);
			});
			it("should spread equally among 3 columns", {
				var baseAngle = Math.PI;
				var sequence = Spell.schedule(ApplicationKind.Spread(1, 0.5, 4, SpreadKind.Wave(3)), MaterialKind.Ink, baseAngle, 0).sure();
				Assert.same({
					time: 0,
					angle: baseAngle
				}, sequence.pop().sure());
				Assert.same({
					time: 2,
					angle: baseAngle + 1 / 2
				}, sequence.pop().sure());
				Assert.same({
					time: 4,
					angle: baseAngle
				}, sequence.pop().sure());
				Assert.same({
					time: 6,
					angle: baseAngle - 1 / 2
				}, sequence.pop().sure());
				sequence.pop().should.be(null);
			});
			it("should spread equally among 4 columns", {
				var baseAngle = Math.PI;
				var sequence = Spell.schedule(ApplicationKind.Spread(1, 1, 6, SpreadKind.Wave(4)), MaterialKind.Ink, baseAngle, 0).sure();
				Assert.same({
					time: 0,
					angle: baseAngle - 1 / 2 + 1 / 3
				}, sequence.pop().sure());
				Assert.same({
					time: 1,
					angle: baseAngle - 1 / 2 + 2 / 3
				}, sequence.pop().sure());
				Assert.same({
					time: 2,
					angle: baseAngle - 1 / 2 + 3 / 3
				}, sequence.pop().sure());
				Assert.same({
					time: 3,
					angle: baseAngle - 1 / 2 + 2 / 3
				}, sequence.pop().sure());
				Assert.same({
					time: 4,
					angle: baseAngle - 1 / 2 + 1 / 3
				}, sequence.pop().sure());
				Assert.same({
					time: 5,
					angle: baseAngle - 1 / 2
				}, sequence.pop().sure());
				sequence.pop().should.be(null);
			});

			it("should correctly group by Simmultaneous wave", {
				var baseAngle = Math.PI;
				var sequence = Spell.schedule(ApplicationKind.Spread(1, 1, 6, SpreadKind.Simultaneous(4)), MaterialKind.Ink, baseAngle, 0).sure();
				Assert.same({
					time: 0,
					angle: baseAngle - 1 / 2
				}, sequence.pop().sure());
				Assert.same({
					time: 0,
					angle: baseAngle - 1 / 2 + 1 / 3
				}, sequence.pop().sure());
				Assert.same({
					time: 0,
					angle: baseAngle - 1 / 2 + 2 / 3
				}, sequence.pop().sure());
				Assert.same({
					time: 0,
					angle: baseAngle - 1 / 2 + 3 / 3
				}, sequence.pop().sure());
				Assert.same({
					time: 1,
					angle: baseAngle - 1 / 2
				}, sequence.pop().sure());
				Assert.same({
					time: 1,
					angle: baseAngle - 1 / 2 + 1 / 3
				}, sequence.pop().sure());
				sequence.pop().should.be(null);
			});
		});
	}
}
