package bacon;

import h2d.Text;
import h2d.TextInput;
import h2d.Graphics;
import bacon.spelling.Spell;
import h2d.filter.Glow;
import h2d.Object;
import tweenxcore.Tools.Easing;
import tweenx909.TweenX;
import h2d.col.Bounds;
import hxd.Key;
import hxd.Event;
import hxd.SceneEvents.Interactive;
import h3d.Engine;
import scenes.IScene;
import tools.debug.Spy;
import tools.ObjectPool;
import bacon.entity.*;

using Safety;
using bacon.entity.Entity;

/**
 * ...
 * @author Samuel Bouchet
 */
@:nullSafety
class MainScene extends h2d.Scene implements IScene {
	private static var SLOWEST = 0.002;
	private static var TRANSITION_DURATION = 0.3;
	private static var TYPING_PENALTY = 0.25;
	public static var instance:Null<h2d.Scene>;

	var app:Main;
	var graphics:h2d.Graphics = new h2d.Graphics();
	var groundTexture:h3d.mat.Texture;
	var paperColor:Int = 0xFFFFFF;
	var timeScale:Float = 1;
	var i:Float = 0;
	var hero:ControlledHero = new ControlledHero(0, 0);
	var enemyPool:Null<ObjectPool<Enemy>>;
	var enemies:List<Enemy> = new List<Enemy>();
	var projectileInkPool:ObjectPool<ProjectileInk> = new ObjectPool<ProjectileInk>(5000, () -> new ProjectileInk(0, 0));
	var projectileInks:List<ProjectileInk> = new List<ProjectileInk>();
	var spells:List<Spell> = new List<Spell>();
	var spellText:String = '';
	var tween:Null<TweenX>;
	var bounds1:Bounds = new Bounds();
	var bounds2:Bounds = new Bounds();
	var waves = [
		[[SpellBook.MOVING_INK]],
		[
			[SpellBook.MOVING_INK],
			[SpellBook.MOVING_INK],
			[
				SpellBook.MOVING_INK,
				SpellBook.MOVING_INK,
				SpellBook.MOVING_INK,
				"Rainiiii Ink",
				"Raini Ink"
			]
		],
		[[SpellBook.RAINING_INK], [SpellBook.RAINING_INK]],
	];
	var waveIdx = -1;

	public function new(app:Main) {
		super();
		this.app = app;

		this.groundTexture = new h3d.mat.Texture(width, height, [Target]);
		new h2d.Bitmap(h2d.Tile.fromTexture(groundTexture), this);

		fromData();

		// Add Player
		addChild(graphics);
		addChild(hero);

		var enemyPool = new ObjectPool<Enemy>(500, () -> new Enemy(0, 0, null, castSpell));
		this.enemyPool = enemyPool;
		instance = this;
	}

	override function onAdd() {
		super.onAdd();

		// live reload
		hxd.Res.data.watch(() -> {
			Data.load(hxd.Res.data.entry.getText());
			AnimationData.fromData();
			fromData();
			hero.fromData();
			for (e in enemies)
				e.fromData();
			for (p in projectileInks)
				p.fromData();
		});

		// add canvas drawing
		redrawGroundCanvas();

		// add raining application
		// add lining application
		// add spreadshot application
		// add biiiiiming application

		// add more spells to enemy
		// add behaviours to enemies

		// add level sequence

		// add slot system: select a slot, enter to manually change, tap the slot to trigger
	}

	inline function redrawGroundCanvas() {
		graphics.clear();
		var tile = hxd.Res.texture.toTile();

		for (x in 0...Math.ceil(width / tile.width))
			for (y in 0...Math.ceil(height / tile.height)) {
				graphics.beginTileFill(x * tile.width, y * tile.height, 1, 1, tile);
				graphics.drawRect(x * tile.width, y * tile.height, tile.width, tile.height);
			}
		graphics.drawTo(groundTexture);
	}

	override function handleEvent(event:Event, last:Interactive):Interactive {
		return super.handleEvent(event, last);
	}

	public function fromData() {
		app.engine.backgroundColor = Data.colors.get(Data.ColorsKind.WARM_DARK).sure().value;
		paperColor = Data.colors.get(Data.ColorsKind.WARM_LIGHT).sure().value;
		var playerStartPos = Data.heros.get(Data.HerosKind.player).sure();
		hero.x = playerStartPos.x;
		hero.y = playerStartPos.y;
	}

	public function update(dt:Float) {
		var alteredDt = dt * timeScale;

		for (spell in spells) {
			if (spell.castSequence.length == 0) {
				spells.remove(spell);
			} else {
				while (spell.castSequence.first() != null && spell.castSequence.first().sure().time <= hxd.Timer.lastTimeStamp) {
					var sequence = spell.castSequence.pop().sure();
					addProjectileInk(spell.caster, sequence.angle, spell.material);
				}
			}
		}

		hero.update(alteredDt, detectCollision);
		updateLayer(hero);
		for (enemy in enemies) {
			enemy.updateMagink(alteredDt, detectCollision, this.graphics, this.groundTexture);
			updateLayer(enemy);
		}
		for (p in projectileInks) {
			p.updateMagink(alteredDt, (_, __, ___) -> null, this.graphics, this.groundTexture);
			updateLayer(p);

			// projectileInk collision check
			updateCollisions(p);
		}
		for (enemy in enemies) {
			if (enemy.lifePoints <= 0 || enemy.parent == null) {
				removeEnemy(enemy);
			}
		}
		checkGameProgress();

		// Spelling behaviour
		if (Inputs.isSpelling) {
			spellText = listenSpellingInput(spellText);
		}

		// Entering or Leaving spelling mode
		if (Inputs.SpellTyping()) {
			Inputs.isSpelling = !Inputs.isSpelling;
			if (Inputs.isSpelling) {
				startSpelling();
			} else {
				finishSpelling();
			}
		}

		hero.tf.text = spellText;

		// DEBUG
		if (Key.isPressed(Key.NUMBER_1)) {
			castSpell(SpellBook.MOVING_INK, hero);
		}
		if (Key.isPressed(Key.NUMPAD_ADD)) {
			timeScale += 0.1;
		}
		if (Key.isPressed(Key.NUMPAD_SUB)) {
			timeScale -= 0.1;
		}
		i = timeScale;

		// Apply canvas modifications to texture
		this.groundTexture.run(groundTexture -> {
			graphics.drawTo(groundTexture);
		});
		graphics.clear();
	}

	private inline function finishSpelling() {
		// cast spell
		var isCasted = castSpell(spellText, hero);

		// animate feather
		if (isCasted) {
			hero.feather.playCast(hero.tf.textWidth * hero.tf.scaleX);
			hero.tf.color.set(0, 0, 0, 1);
			hero.tf.textColor = 0x960B0A;
		} else {
			hero.feather.playWrong(hero.tf.textWidth * hero.tf.scaleX);
			drawScratch(hero.tf, graphics);
		}
		// draw casted spell on ground and reset the spell text
		groundTexture.run(gt -> hero.tf.drawTo(gt));
		spellText = "";

		// Ensure we get back in 1× time scale
		TweenX.clear();
		TweenX.tweenFunc1((scale) -> timeScale = scale, SLOWEST, 1).time(TRANSITION_DURATION * 0.5).ease(Easing.quadOut);
	}

	private inline function startSpelling() {
		// slow mo + black text
		TweenX.tweenFunc1((scale) -> timeScale = scale, 1, SLOWEST).time(TRANSITION_DURATION).ease(Easing.quadIn);
		hero.tf.color.set(0, 0, 0, 1);
		hero.tf.textColor = 0x000000;
	}

	private inline function drawScratch(tf:Text, graphics:Graphics):Void {
		graphics.beginFill(0, 1);
		graphics.lineStyle(2, 0x000000, 1);
		graphics.moveTo(tf.absX - Math.random() * 10, tf.absY + 22 - Math.random() * 8);
		graphics.lineTo(tf.absX + tf.textWidth * tf.scaleX + Math.random() * 10, tf.absY + 22 - Math.random() * 8);
		graphics.lineTo(tf.absX - Math.random() * 10, tf.absY + 22 - Math.random() * 8);
		graphics.lineTo(tf.absX + tf.textWidth * tf.scaleX + Math.random() * 10, tf.absY + 22 - Math.random() * 8);
		graphics.endFill();
	}

	private function listenSpellingInput(spellText:String):String {
		for (k in Key.A...Key.Z) {
			if (Key.isPressed(k)) {
				this.tween = TweenX.tweenFunc1((scale) -> timeScale = scale, SLOWEST, 0.3)
					.repeat(2)
					.time(TYPING_PENALTY)
					.ease(Easing.quadOut)
					.yoyo();
				return spellText + (Key.isDown(Key.SHIFT) ? Key.getKeyName(k).or('') : Key.getKeyName(k).or('').toLowerCase());
			}
		}
		if (Key.isPressed(Key.SPACE)) {
			return spellText + ' ';
		}
		if (Key.isPressed(Key.DELETE) || Key.isPressed(Key.BACKSPACE)) {
			return spellText.substr(0, spellText.length - 1);
		}
		return spellText;
	}

	private inline function checkGameProgress() {
		if (enemies.length == 0) {
			waveIdx++;
			if (waveIdx >= waves.length) {
				spellText = "Victory !";
			} else {
				var next = waves[waveIdx];
				for (i in 0...next.length) {
					addEnemy(next[i]);
				}
			}
		}
		if (hero.lifePoints <= 0 && spellText != "Game Over") {
			spellText = "Game Over";
			Inputs.isSpelling = true;
			TweenX.tweenFunc1((scale) -> timeScale = scale, 1, SLOWEST).time(TRANSITION_DURATION).ease(Easing.quadIn);
			hero.tf.color.set(0, 0, 0, 1);
		}
	}

	private function addProjectileInk(from:IEntity, direction:Float, material:MaterialKind) {
		var p = projectileInkPool.sure().getItem();
		addChild(p);
		p.direction = direction;
		p.material = material;
		var fromHitboxY = from.y + from.hitbox.yMin + from.hitbox.height / 2;
		var pHitboxY = p.hitbox.yMin + p.hitbox.height / 2;
		var avoidCollisionOffset = (p.hitbox.width / 2 + from.hitbox.width / 2) * 1.1;
		p.x = from.x + from.hitbox.xMin + from.hitbox.width / 2 + avoidCollisionOffset * Math.cos(p.direction);
		p.y = fromHitboxY + avoidCollisionOffset * Math.sin(p.direction) - pHitboxY;
		p.lifePoints = 1;
		projectileInks.push(p);
	}

	private function removeProjectileInk(p:ProjectileInk) {
		projectileInks.remove(p);
		p.cleanup();
		removeChild(p);
		projectileInkPool.sure().putItem(p);
	}

	private function addEnemy(spells:Array<String>) {
		var e = enemyPool.sure().getItem();
		e.spells = spells;
		e.x = 400 + Math.random() * 500;
		e.y = Math.random() * 500;
		e.lifePoints = 3;
		addChild(e);
		enemies.add(e);
	}

	private function removeEnemy(e:Enemy) {
		enemies.remove(e);
		removeChild(e);
		enemyPool.sure().putItem(e);
	}

	private inline function updateCollisions(p:ProjectileInk) {
		bounds1.load(p.hitbox);
		bounds1.offset(p.x, p.y);
		// out of screen
		bounds2.set(0, 0, width, height);
		if (!bounds1.intersects(bounds2)) {
			trace("out of screen !" + p.y);
			p.lifePoints = 0;
		}
		// hit enemy
		for (enemy in enemies) {
			bounds2.load(enemy.hitbox);
			bounds2.offset(enemy.x, enemy.y);
			if (bounds1.intersects(bounds2)) {
				trace("enemy hit !");
				p.lifePoints--;
				switch (p.material) {
					case Ink:
						enemy.lifePoints--;
						enemy.lifeBar.playHit(enemy.lifePoints);
					case Paper:
						enemy.castingAcc = 0;
					default:
				}
			}
		}
		// hit hero
		bounds2.load(hero.hitbox);
		bounds2.offset(hero.x, hero.y);
		if (bounds1.intersects(bounds2)) {
			trace("hero hit !");
			p.lifePoints--;
			hero.lifePoints--;
			hero.lifeBar.run(lifeBar -> lifeBar.playHit(hero.lifePoints));
		}

		if (p.lifePoints == 0 || p.parent == null) {
			trace("try remove P lifePoints=" + p.lifePoints);
			projectileInks.remove(p); // prevent further updates and collisions
			p.destroy(removeProjectileInk, this.graphics, this.groundTexture);
			addChildAt(p, 1000); // animate the projectileInk explosion above others
		}
	}

	private inline function updateLayer(object:IEntity) {
		var layer = Std.int(Math.abs(object.y + object.hitbox.yMin) / 4);
		return addChildAt(cast object, layer);
	}

	function castSpell(text:String, from:IEntity):Bool {
		var spell = Spell.build(text, from, from == hero ? -Math.PI / 2 : Math.PI / 2);

		if (spell != null) {
			spells.add(spell.sure());
			return true;
		}
		if (text.toLowerCase() == "game over") {
			this.app.setScene(new MainScene(app), true);
			return true;
		}
		return false;
	}

	function detectCollision(entity:Object, xOffset:Float, yoffset:Float):Null<Object> {
		return cast(entity, IEntity).let(function(entity:IEntity):Null<Object> {
			bounds1.load(entity.hitbox);
			bounds1.offset(entity.x, entity.y);
			bounds1.offset(xOffset, yoffset);
			for (enemy in enemies) {
				if (entity != enemy) {
					bounds2.load(enemy.hitbox);
					bounds2.offset(enemy.x, enemy.y);
					if (bounds1.intersects(bounds2)) {
						return cast(enemy, Object);
					}
				}
			}
			if (entity != hero) {
				bounds2.load(hero.hitbox);
				bounds2.offset(hero.x, hero.y);
				if (bounds1.intersects(bounds2)) {
					return cast(hero, Object);
				}
			}
			return null;
		});
	}
}
