package bacon;

import tools.anim.AnimatedSprite.Sequence;
import haxe.ds.StringMap;

class AnimationData {
	public static var data:Map<Data.AnimsKind,AnimationData> = new Map<Data.AnimsKind,AnimationData>();

	public var sequences:StringMap<Sequence>;
	public var tiles:Array<h2d.Tile>;

	public function new(sequences:StringMap<Sequence>, tiles:Array<h2d.Tile>) {
		this.sequences = sequences;
		this.tiles = tiles;
	}

	public static function fromData() {
		for (anim in Data.anims.all) {
			var tiles = hxd.Res.load(anim.spritesheet).toTile().split(anim.tileCount);
			for (tile in tiles) {
				tile.setCenterRatio();
			}
			var sequences = Anims.parseData(anim);
			AnimationData.data.set(anim.Id, new AnimationData(sequences, tiles));
		}
	}
}
