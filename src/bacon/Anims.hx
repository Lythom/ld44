package bacon;

import haxe.Json;
import haxe.ds.StringMap;
import tools.anim.AnimatedSprite.Sequence;


@:enum
abstract SequenceKey(String) from String to String {
	var Iddle = 'Iddle';
	var Run = 'Run';
	var Jump = 'Jump';
	var Explode = 'Explode';
	var Swap = 'Swap';
}

class Anims {
    public static function parseSequence(seq:{
		expositions:String,
		framesIdx:String
	}):Sequence {
		var expo = Json.parse(seq.expositions);
		return ({
			framesIdx: Json.parse(seq.framesIdx),
			expositions: (Std.is(expo, Float) ? Single(cast(expo, Float)) : List(cast expo))
		});
	}

	public static function parseData(anim:Data.Anims):StringMap<Sequence> {
		var result = new StringMap<Sequence>();
		for (seqData in anim.sequences) {
			var seq = parseSequence({expositions: seqData.expositions, framesIdx: seqData.framesIdx});
			var transitionFrom:Null<StringMap<Sequence>> = null;
			if (seqData.transitionFrom.length > 0) {
				transitionFrom = new StringMap<Sequence>();
				for (tf in seqData.transitionFrom) {
					transitionFrom.set(tf.fromSequence, parseSequence({expositions: tf.expositions, framesIdx: tf.framesIdx}));
				}
			}
			seq.transitionFrom = transitionFrom;
			result.set(seqData.name.toString(), seq);
		}
		return result;
	}

}