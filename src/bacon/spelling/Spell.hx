package bacon.spelling;

import bacon.entity.Entity.IEntity;
import haxe.ds.IntMap;

using Safety;

@:enum
abstract MaterialKind(Data.AnimsKind) to Data.AnimsKind {
	var Ink = Data.AnimsKind.Ink;
	var Paper = Data.AnimsKind.Paper;
}

enum SpreadKind {
	Random;
	Wave(columnCount:Int);
	Simultaneous(columnCount:Int);
}

enum ApplicationKind {
	/**
	 * @param rate Number of projection per second
	 * @param count Total of projectiles launched
	 */
	Mov(rate:Float, count:Int);

	/**
	 * @param angleRange width of the spreading
	 * @param rate Number of projection per second
	 * @param count Total of projectiles launched
	 * @param kind Kind of spread
	 */
	Spread(angleRange:Float, rate:Float, count:Int, kind:SpreadKind);
}

typedef ProjectileThrow = {
	var time:Float;
	var angle:Float; // in Radiants, 0 means to the right.
}

class Spell {
	public var castSequence:List<ProjectileThrow>;
	public var material:MaterialKind;
	public var caster:IEntity;

	public function new(caster:IEntity, castSequence:List<ProjectileThrow>, material:MaterialKind) {
		this.caster = caster;
		this.castSequence = castSequence;
		this.material = material;
	}

	public static function build(spellText:String, caster:IEntity, direction:Float):Null<Spell> {
		var spellParts = spellText.toLowerCase().split(" ");
		if (spellParts.length != 2)
			return null;

		var application:Null<ApplicationKind> = null;
		var material:Null<MaterialKind> = null;
		application = switch (spellParts[0]) {
			case "mov": ApplicationKind.Mov(1, 1);
			case "rain": ApplicationKind.Spread(Math.PI / 4, 8, 8, SpreadKind.Random);
			case "wave": ApplicationKind.Spread(Math.PI * 2 - Math.PI / 4, 3, 3 * 8, SpreadKind.Simultaneous(8));
			case "tourniquet": ApplicationKind.Spread(Math.PI * 0.8, 8, 3 * 8, SpreadKind.Wave(8));
			default: null;
		}

		material = switch (spellParts[1]) {
			case "ink": MaterialKind.Ink;
			case "paper": MaterialKind.Paper;
			default: null;
		}

		if (application == null || material == null)
			return null;

		var castSequence = schedule(application, material, direction, hxd.Timer.lastTimeStamp);
		if (castSequence.length == 0)
			return null;

		return new Spell(caster, castSequence.sure(), material.sure());
	}

	public static function schedule(application:ApplicationKind, materiel:MaterialKind, baseAngle:Float, lastTimeStamp:Float):List<ProjectileThrow> {
		var castSequence = new List<ProjectileThrow>();

		switch (application) {
			case Mov(rate, count):
				for (i in 0...count) {
					castSequence.add({
						time: lastTimeStamp + i / rate,
						angle: baseAngle,
					});
				}
			case Spread(angleRange, rate, count, kind):
				if (rate == 0)
					return castSequence;
				switch (kind) {
					case Random:
						for (i in 0...count) {
							castSequence.add({
								time: lastTimeStamp + i / rate,
								angle: baseAngle - angleRange / 2 + Math.random() * angleRange
							});
						}
					case Wave(columnCount):
						if (columnCount == 0) return castSequence;
						scheduleSpread(castSequence, count, rate, baseAngle, angleRange, columnCount, lastTimeStamp);

					case Simultaneous(columnCount):
						if (columnCount == 0) return castSequence;
						scheduleSimultaneousSpread(castSequence, count, rate, baseAngle, angleRange, columnCount, lastTimeStamp);
				}
		}

		return castSequence;
	}

	public static inline function scheduleSpread(castSequence:List<ProjectileThrow>, count:Int, rate:Float, baseAngle:Float, angleRange:Float,
			columnCount:Int, lastTimeStamp:Float):Void {
		var minAngle = baseAngle - angleRange / 2;
		var maxAngle = baseAngle + angleRange / 2;
		var incrementBy = columnCount == 1 ? 0 : angleRange / (columnCount - 1);
		var currAngle = columnCount % 2 == 1 ? baseAngle : minAngle + incrementBy * Std.int((columnCount - 1) / 2); // start half way

		for (i in 0...count) {
			castSequence.add({
				time: lastTimeStamp + i / rate,
				angle: currAngle
			});
			currAngle += incrementBy;
			if (currAngle >= maxAngle || currAngle <= minAngle)
				incrementBy = -incrementBy;
		}
	}

	public static inline function scheduleSimultaneousSpread(castSequence:List<ProjectileThrow>, count:Int, rate:Float, baseAngle:Float, angleRange:Float,
			columnCount:Int, lastTimeStamp:Float):Void {
		var minAngle = baseAngle - angleRange / 2;
		var incrementBy = angleRange / (columnCount - 1);

		for (i in 0...count) {
			castSequence.add({
				time: lastTimeStamp + Std.int(i / columnCount) / rate,
				angle: columnCount == 1 ? baseAngle : minAngle + (i % columnCount) * incrementBy
			});
		}
	}
}
