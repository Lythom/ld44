package bacon.entity;

import h2d.Bitmap;
import h3d.mat.Texture;
import h2d.Graphics;
import h2d.Object;

using tweenxcore.Tools;
using Safety;

class LifeBar extends h2d.Bitmap {
	var maxLifePoints:Float = 3;
	var currentAnim:String = "";
	var acc:Float = 0;
	var graphics:Graphics = new Graphics();
	var groundTexture:Texture;
	var scaling:Float = 1;

	public function new(maxLifePoints:Float, scaling:Float, ?parent:h2d.Object) {
		var tile = hxd.Res.bookmark.toTile();
		tile.scaleToSize(Std.int(tile.width * scaling), Std.int(tile.height * scaling));
		this.groundTexture = new h3d.mat.Texture(Std.int(tile.width), Std.int(tile.height), [Target]);
		new Bitmap(tile).drawTo(this.groundTexture);
		super(h2d.Tile.fromTexture(groundTexture), parent);
		tile.setCenterRatio();
		this.maxLifePoints = maxLifePoints;
		this.scaling = scaling;
	}

	override function onAdd() {
		super.onAdd();
		graphics.clear();
		graphics.endFill();
		graphics.lineStyle(1, 0xFF960B0A, 1);
		for (i in 0...Math.round(maxLifePoints)) {
			var y = (maxLifePoints - i) * (tile.height * 0.8) / maxLifePoints + tile.dy;
			graphics.drawRect(tile.width * 0.25 + tile.dx, y, tile.width * 0.5, tile.width * 0.5);
		}
		graphics.drawTo(this.groundTexture);
	}

	public function update(dt:Float) {
		acc += dt;
		if (currentAnim == "hit") {
			var duration = 0.3;
			var progress = (acc / duration);
			this.rotation = hitEase(progress) * 0.2;
			if (progress >= 1) {
				resetToDefaults();
			}
		}
	}

	function resetToDefaults() {
		this.rotation = 0;
		currentAnim = "";
	}

	public function playHit(lifePoints:Float) {
		currentAnim = "hit";
		acc = 0;
		var w = 6 + Math.random() * 6 * scaling;
		var x = tile.width * 0.5 + tile.dx;
		var y = (maxLifePoints - lifePoints) * (tile.height * 0.8) / maxLifePoints + tile.dy;
		graphics.clear();
		graphics.beginFill(Data.colors.get(Data.ColorsKind.COLD_DARK).sure().value);
		graphics.drawEllipse(x + Math.random() * 10 - 5, y, w, w * 0.8);
		w = 5 + Math.random() * 5 * scaling;
		graphics.drawEllipse(x + Math.random() * 10 - 5, y + 5, w, w * 0.8);
		w = 5 + Math.random() * 5 * scaling;
		graphics.drawEllipse(x + Math.random() * 10 - 5, y + 10, w, w * 0.8);
		graphics.endFill();
		graphics.drawTo(this.groundTexture);
	}

	function hitEase(rate:Float):Float {
		// http://tweenx.spheresofa.net/core/custom/#%7B%22time%22%3A0.3%2C%22easing%22%3A%5B%22Op%22%2C%5B%22Op%22%2C%5B%22Simple%22%2C%5B%22Standard%22%2C%22Quad%22%2C%22OutIn%22%5D%5D%2C%5B%22Lerp%22%2C1.1%2C0.64%5D%5D%2C%5B%22Op%22%2C%5B%22Op%22%2C%5B%22Op%22%2C%5B%22Simple%22%2C%5B%22Standard%22%2C%22Quad%22%2C%22Out%22%5D%5D%2C%5B%22RoundTrip%22%2C%22Yoyo%22%5D%5D%2C%5B%22Repeat%22%2C2%5D%5D%2C%22Multiply%22%5D%5D%7D
		return tweenxcore.Tools.FloatTools.lerp(tweenxcore.Tools.Easing.quadOutIn(rate), 1.1, 0.64) * tweenxcore.Tools.FloatTools.yoyo(tweenxcore.Tools
			.FloatTools.repeat(tweenxcore.Tools.FloatTools.lerp(rate, 0, 2), 0, 1),
			tweenxcore.Tools.Easing.quadOut);
	}

	function castEase(rate:Float):Float {
		return tweenxcore.Tools.FloatTools.zigzag(rate, tweenxcore.Tools.Easing.quadOut);
	}
}
