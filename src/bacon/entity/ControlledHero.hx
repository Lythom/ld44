package bacon.entity;

import bacon.entity.Entity.IEntity;
import h2d.filter.Glow;
import h2d.Text;
import bacon.Anims;
import platformer.BasicPhysicsObject;
import haxe.ds.StringMap;
import tools.anim.AnimatedSprite;
import h2d.col.Bounds;
import h2d.Object;
import h2d.Bitmap;

using Safety;
using tweenxcore.Tools;

/**
 * ...
 * @author Lythom
 */
class ControlledHero extends BasicPhysicsObject implements IEntity {
	public var hitbox(default, null):Bounds = Bounds.fromValues(0, 0, 0, 0);
	public var anim:Null<AnimatedSprite>;
	public var tf:Text;
	public var feather:Feather;
	public var lifeBar:Null<LifeBar>;
	public var lifePoints:Int = 3;

	var glowFilter = new Glow();
	var heroFriction:Float = 0.2;
	var heroMoveSpeedFactor:Float = 1;
	var heroFontScale:Float = 4;

	public function new(x:Float = 0, y:Float = 0, ?parent:Object) {
		super(x, y, parent);
		this.feather = new Feather();
		this.tf = new Text(hxd.res.DefaultFont.get());
		fromData();
	}

	override function onAdd() {
		super.onAdd();
	}

	public function fromData() {
		this.removeChildren();

		heroFriction = Data.config.get(Data.ConfigKind.heroFriction).sure().value;
		heroMoveSpeedFactor = Data.config.get(Data.ConfigKind.heroMoveSpeedFactor).sure().value;
		heroFontScale = Data.config.get(Data.ConfigKind.heroFontScale).sure().value;
		var heroShadowOffset = Data.config.get(Data.ConfigKind.heroShadowOffset).sure().value;

		// Yay load from Data !
		// get Data
		var animData:Data.Anims = Data.anims.get(Data.AnimsKind.Hero).sure();
		// load tiles from file reference
		var tiles = hxd.Res.load(animData.spritesheet).toTile().split(animData.tileCount); // // hxd.Res.Test_sprite00000_11x1.toTile().split(11);
		// parse sequences
		var sequences:StringMap<Sequence> = Anims.parseData(animData);
		for (tile in tiles) {
			tile.setCenterRatio();
		}

		var anim = new AnimatedSprite(tiles, sequences, 12, this);
		anim.play(SequenceKey.Run);
		this.anim = anim;

		tf.text = "";
		tf.y = -80;
		tf.x = -40;
		tf.color.set(0, 0, 0, 1);
		tf.textColor = 0x000000;
		addChild(tf);

		var shadow:Bitmap = new Bitmap(hxd.Res.shadow.toTile());
		shadow.smooth = true;
		shadow.tile.setCenterRatio();
		shadow.tile.dy = Std.int(shadow.tile.dy + heroShadowOffset);
		shadow.setScale(tiles[0].width * 0.8 / shadow.tile.width);
		addChildAt(shadow, 0);
		this.hitbox = Bounds.fromValues(shadow.tile.dx * shadow.scaleX, shadow.tile.dy * shadow.scaleX, tiles[0].width * 0.8,
			tiles[0].width * 0.8 * shadow.tile.height / shadow.tile.width);

		glowFilter.color = 0x960B0A;
		glowFilter.alpha = 0;
		glowFilter.smoothColor = true;
		glowFilter.radius = 40;
		glowFilter.gain = 1.04;
		glowFilter.linear = 0.2;
		glowFilter.quality = 0.5;
		anim.filter = glowFilter;

		addChildAt(this.feather, 2);

		var lifeBar:LifeBar = new LifeBar(3, 1);
		addChildAt(lifeBar, 30);
		this.lifeBar = lifeBar;
		lifeBar.x = tiles[0].width * 0.5;
		lifeBar.y = -tiles[0].height * 0.5 + 10;

		// var bmpPerso:Bitmap = new Bitmap(h2d.Tile.fromColor(0xFFFFFF, Std.int(this.hitbox.width), Std.int(this.hitbox.height)), this);
		// bmpPerso.color = new h3d.Vector(1, 0.2, 1, 0.5);
		// bmpPerso.tile.dx = Std.int(this.hitbox.x);
		// bmpPerso.tile.dy = Std.int(this.hitbox.y);
	}

	override public function update(dt:Float, collide:DetectCollision, forceFriction:Bool = false):Void {
		this.acceleration.set(0, 0);
		var xAcc:Float = 4000;
		var yAcc:Float = 4000;

		maxVelocity.x = 600 * heroMoveSpeedFactor;
		maxVelocity.y = 400 * heroMoveSpeedFactor;

		tf.setScale(heroFontScale);
		feather.update(dt, this.tf);

		this.lifeBar.run(lb -> lb.update(dt));

		if (Inputs.MoveUp()) {
			this.acceleration.y = -yAcc * heroMoveSpeedFactor;
			friction.y = 0;
		} else if (Inputs.MoveDown()) {
			this.acceleration.y = yAcc * heroMoveSpeedFactor;
			friction.y = 0;
		} else {
			friction.y = heroFriction;
		}

		if (Inputs.MoveLeft()) {
			this.acceleration.x = -xAcc * heroMoveSpeedFactor;
			friction.x = 0;
			anim.run(anim -> {
				if (anim.scaleX > 0)
					anim.scaleX = anim.scaleX * -1;
			});
		} else if (Inputs.MoveRight()) {
			this.acceleration.x = xAcc * heroMoveSpeedFactor;
			friction.x = 0;
			anim.run(anim -> {
				if (anim.scaleX < 0)
					anim.scaleX = anim.scaleX * -1;
			});
		} else {
			friction.x = heroFriction;
		}

		anim.run(anim -> {
			if (Inputs.isSpelling) {
				anim.play(SequenceKey.Jump);
				glowFilter.alpha = glowFilter.alpha + (1 - glowFilter.alpha) * 0.1;
			} else {
				if (!Inputs.MoveLeft() && !Inputs.MoveRight() && !Inputs.MoveUp() && !Inputs.MoveDown()) {
					anim.play(SequenceKey.Iddle);
				} else if ((Inputs.MoveLeft() && velocity.x > 0) || (Inputs.MoveRight() && velocity.x < 0)) {
					anim.play(SequenceKey.Swap);
				} else {
					anim.play(SequenceKey.Run);
				}
				glowFilter.alpha = glowFilter.alpha + (0 - glowFilter.alpha) * 0.1;
			}
			anim.speed = 15 * dt / hxd.Timer.dt;
		});

		super.update(dt, collide, true);
	}
}
