package bacon.entity;

import bacon.entity.Entity.IEntity;
import h2d.Graphics;
import h2d.Text;
import tweenx909.TweenX;
import h2d.col.Point;
import platformer.BasicPhysicsObject;
import bacon.Anims;
import haxe.ds.StringMap;
import tools.anim.AnimatedSprite;
import h2d.col.Bounds;
import h2d.Object;
import h2d.Bitmap;

using Safety;
using tweenxcore.Tools;

/**
 * ...
 * @author Lythom
 */
class Enemy extends BasicPhysicsObject implements IEntity {
	public var hitbox(default, null):Bounds = Bounds.fromValues(0, 0, 0, 0);
	public var anim:Null<AnimatedSprite>;
	public var tf:Text;
	public var targetPosition:h2d.col.Point;
	public var lifePoints:Int = 3;
	public var lifeBar:LifeBar;
	public var feather:Feather;
	public var spells:Array<String> = [];

	public var castingAcc:Float = 0;
	var currentSpell:Null<String> = null;
	var castSpell:String->IEntity->Bool;
	var tween:Null<TweenX>;
	var enemyMoveSpeedFactor:Float = 1;
	var enemyAccelerationFactor:Float = 1;
	var enemyTypingDuration:Float = 0.15;
	var enemyFontScale:Float = 3;

	public function new(x:Float = 0, y:Float = 0, ?parent:Object, castSpell:String->IEntity->Bool) {
		super(x, y, parent);
		this.castSpell = castSpell;
		this.targetPosition = new Point(100 + Math.random() * 800, 100 + Math.random() * 500);
		this.tf = new Text(hxd.res.DefaultFont.get());
		this.feather = new Feather();
		this.lifeBar = new LifeBar(3, 0.5);
		fromData();
	}

	override function onAdd() {
		super.onAdd();
	}

	public function fromData() {
		this.removeChildren();

		var enemyShadowOffset = Data.config.get(Data.ConfigKind.enemyShadowOffset).sure().value;
		enemyMoveSpeedFactor = Data.config.get(Data.ConfigKind.enemyMoveSpeedFactor).sure().value;
		enemyAccelerationFactor = Data.config.get(Data.ConfigKind.enemyAccelerationFactor).sure().value;
		enemyTypingDuration = Data.config.get(Data.ConfigKind.enemyTypingDuration).sure().value;
		enemyFontScale = Data.config.get(Data.ConfigKind.enemyFontScale).sure().value;

		// Yay load from Data !
		// get Data
		var animData:Data.Anims = Data.anims.get(Data.AnimsKind.Enemy).sure();
		// load tiles from file reference
		var tiles = hxd.Res.load(animData.spritesheet).toTile().split(animData.tileCount); // // hxd.Res.Test_sprite00000_11x1.toTile().split(11);
		// parse sequences
		var sequences:StringMap<Sequence> = Anims.parseData(animData);
		for (tile in tiles) {
			tile.setCenterRatio();
		}

		var anim = new AnimatedSprite(tiles, sequences, 12, this);
		this.hitbox = Bounds.fromValues(tiles[0].dx, tiles[0].dy, tiles[0].width, tiles[0].height);
		anim.play(SequenceKey.Run);
		this.anim = anim;

		var shadow:Bitmap = new Bitmap(hxd.Res.shadow.toTile());
		shadow.smooth = true;
		shadow.tile.setCenterRatio();
		shadow.tile.dy = Std.int(shadow.tile.dy + enemyShadowOffset);
		shadow.setScale(tiles[0].width * 0.8 / shadow.tile.width);
		addChildAt(shadow, 0);
		this.hitbox = Bounds.fromValues(shadow.tile.dx * shadow.scaleX, shadow.tile.dy * shadow.scaleX, tiles[0].width * 0.8,
			tiles[0].width * 0.8 * shadow.tile.height / shadow.tile.width);

		// var bmpPerso:Bitmap = new Bitmap(h2d.Tile.fromColor(0xFFFFFF, Std.int(this.hitbox.width), Std.int(this.hitbox.height)), this);
		// bmpPerso.color = new h3d.Vector(1, 1, 1, 0.3);
		// bmpPerso.tile.dx = Std.int(this.hitbox.x);
		// bmpPerso.tile.dy = Std.int(this.hitbox.y);

		tf.text = "";
		tf.x = -40;
		tf.y = 50;
		addChild(tf);

		addChildAt(feather, 2);

		addChildAt(lifeBar, 0);
		lifeBar.x = tiles[0].width * 0.5 - 10;
		lifeBar.y = -tiles[0].height * 0.5 + 10;
	}

	public function updateMagink(dt:Float, collide:DetectCollision, graphics:Graphics, groundTexture:h3d.mat.Texture):Void {
		this.acceleration.set(0, 0);
		var xAcc:Float = 1000 * enemyAccelerationFactor;
		var yAcc:Float = 1000 * enemyAccelerationFactor;
		maxVelocity.x = 600 * enemyMoveSpeedFactor;
		maxVelocity.y = 400 * enemyMoveSpeedFactor;

		tf.setScale(enemyFontScale);
		feather.update(dt, this.tf);

		this.lifeBar.run(lb -> lb.update(dt));

		// Can be moving or casting
		// is moving to target when he have no spell
		if (currentSpell == null) {
			if (Math.abs(this.x - targetPosition.x) < 30 && Math.abs(this.y - targetPosition.y) < 30) {
				currentSpell = spells[Std.int(Math.random() * spells.length)];
				tf.color.set(0, 0, 0, 1);
			}

			var moveUp = false;
			var moveDown = false;
			var moveRight = false;
			var moveLeft = false;
			if (targetPosition.y < this.y) {
				moveUp = true;
			} else if (targetPosition.y > this.y) {
				moveDown = true;
			}
			if (targetPosition.x < this.x) {
				moveLeft = true;
			} else if (targetPosition.x > this.x) {
				moveRight = true;
			}

			if (moveUp) {
				this.acceleration.y = -yAcc;
				friction.y = 0;
			} else if (moveDown) {
				this.acceleration.y = yAcc;
				friction.y = 0;
			} else {
				friction.y = 1;
			}

			if (moveLeft) {
				this.acceleration.x = -xAcc;
				friction.x = 0;
				anim.run(anim -> {
					if (anim.scaleX > 0)
						anim.scaleX = anim.scaleX * -1;
				});
			} else if (moveRight) {
				this.acceleration.x = xAcc;
				friction.x = 0;
				anim.run(anim -> {
					if (anim.scaleX < 0)
						anim.scaleX = anim.scaleX * -1;
				});
			} else {
				friction.x = 1;
			}

			anim.run(anim -> {
				if (!moveLeft && !moveRight && !moveUp && !moveDown) {
					anim.play(SequenceKey.Iddle);
				} else if ((moveLeft && velocity.x > 0) || (moveRight && velocity.x < 0)) {
					anim.play(SequenceKey.Swap);
				} else {
					anim.play(SequenceKey.Run);
				}
				anim.speed = 15 * dt / hxd.Timer.dt;
			});
		} else {
			this.acceleration.set(0, 0);
			this.friction.set(0.2, 0.2);

			// is casting
			currentSpell.run(currentSpell -> {
				castingAcc += dt;
				var progress = castingAcc / (enemyTypingDuration * currentSpell.length + 0.3);
				this.tf.run(tf -> {
					tf.text = currentSpell.substring(0, Math.ceil(progress * currentSpell.length));
					if (progress >= 1) {
						var casted = castSpell(currentSpell, this);

						if (casted) {
							feather.playCast(tf.textWidth * tf.scaleX);
							tf.color.setColor(0xFF960B0A);
						} else {
							feather.playWrong(tf.textWidth * tf.scaleX);
							graphics.beginFill(0, 1);
							graphics.lineStyle(2, 0x000000, 1);
							graphics.moveTo(tf.absX - Math.random() * 10, tf.absY + 22 - Math.random() * 8);
							graphics.lineTo(tf.absX + tf.textWidth * tf.scaleX + Math.random() * 10, tf.absY + 22 - Math.random() * 8);
							graphics.lineTo(tf.absX - Math.random() * 10, tf.absY + 22 - Math.random() * 8);
							graphics.lineTo(tf.absX + tf.textWidth * tf.scaleX + Math.random() * 10, tf.absY + 22 - Math.random() * 8);
							graphics.endFill();
						}
						tf.drawTo(groundTexture);

						this.currentSpell = null;
						targetPosition = new Point(50 + Math.random() * 1000, Math.random() * 500);
						castingAcc = 0;
						tf.text = "";
					}
				});
			});
			anim.run(anim -> {
				anim.play(SequenceKey.Jump);
				anim.speed = 15 * dt / hxd.Timer.dt;
			});
		}
		update(dt, collide);
	}

	override public function update(dt:Float, collide:DetectCollision, forceFriction:Bool = false):Void {
		var forceFriction = currentSpell != null;
		super.update(dt, collide, forceFriction);
	}
}
