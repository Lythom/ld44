package bacon.entity;

using Safety;

interface IEntity {
	public var hitbox(default, null):h2d.col.Bounds;
	public var x(default, set):Float;
	public var y(default, set):Float;
	public var lifePoints:Int;
}

class Entity {
	public static function updateLayer(object:h2d.Object):Void {
		cast(object, IEntity).run(entity -> {
			var layer = Std.int(Math.abs(entity.y + entity.hitbox.yMin) / 4);
			object.parent.run(object -> object.addChildAt(object, layer));
		});
	}
}
