package bacon.entity;

import h2d.Graphics;
import bacon.Anims;
import platformer.BasicPhysicsObject;
import haxe.ds.StringMap;
import tools.anim.AnimatedSprite;
import h2d.col.Bounds;
import h2d.Object;
import h2d.Bitmap;
import bacon.entity.Entity.IEntity;

using Safety;

/**
 * ...
 * @author Lythom
 */
class ProjectileInk extends BasicPhysicsObject implements IEntity {
	public var hitbox(default, null):Bounds = Bounds.fromValues(0, 0, 0, 0);
	public var anim:Null<AnimatedSprite>;
	public var direction:Float = 1;
	public var lifePoints:Int = 1;

	var projectileMoveSpeedFactor:Float = 1;
	var printCooldown = 0;

	public function new(x:Float = 0, y:Float = 0, direction:Int = 1, ?parent:Object) {
		super(x, y, parent);
		this.direction = direction;
		fromData();
	}

	override function onAdd() {
		super.onAdd();
	}

	public function fromData() {
		this.removeChildren();

		projectileMoveSpeedFactor = Data.config.get(Data.ConfigKind.projectileMoveSpeedFactor).sure().value;
		var projectileShadowOffset = Data.config.get(Data.ConfigKind.projectileShadowOffset).sure().value;

		// Yay load from Data !
		// get Data
		var animData:Data.Anims = Data.anims.get(Data.AnimsKind.Paper).sure();
		// load tiles from file reference
		var tiles = hxd.Res.load(animData.spritesheet).toTile().split(animData.tileCount); // // hxd.Res.Test_sprite00000_11x1.toTile().split(11);
		// parse sequences
		var sequences:StringMap<Sequence> = Anims.parseData(animData);
		for (tile in tiles) {
			tile.setCenterRatio();
		}

		var anim = new AnimatedSprite(tiles, sequences, 15, this);
		anim.play(SequenceKey.Iddle);
		this.anim = anim;

		var shadow:Bitmap = new Bitmap(hxd.Res.shadow.toTile());
		shadow.smooth = true;
		shadow.tile.setCenterRatio();
		shadow.tile.dy = Std.int(shadow.tile.dy + projectileShadowOffset);
		shadow.setScale(tiles[0].width * 0.9 / shadow.tile.width);
		addChildAt(shadow, 0);
		this.hitbox = Bounds.fromValues(shadow.tile.dx * shadow.scaleX, shadow.tile.dy * shadow.scaleX, tiles[0].width * 0.8,
			tiles[0].width * 0.8 * shadow.tile.height / shadow.tile.width);

		// var bmpPerso:Bitmap = new Bitmap(h2d.Tile.fromColor(0xFFFFFF, Std.int(this.hitbox.width), Std.int(this.hitbox.height)), this);
		// bmpPerso.color = new h3d.Vector(1, 1, 1, 0.3);
		// bmpPerso.tile.dx = Std.int(this.hitbox.x);
		// bmpPerso.tile.dy = Std.int(this.hitbox.y);
	}

	public function updateMagink(dt:Float, collide:DetectCollision, texture:h3d.mat.Texture):Void {
		update(dt, collide);
		printCooldown += dt;
		if (printCooldown >= 1/6) {
			printCooldown -= 1/6;
			this.drawTo(texture);
		}
	}

	public function destroy(onDestroy:Projectile->Void, texture:h3d.mat.Texture) {
		anim.run(anim -> {
			anim.play(SequenceKey.Explode, 0);
			anim.loop = false;
			anim.onAnimEnd = () -> {
				this.drawTo(texture);
				onDestroy(this);
			}
		});
	}

	override public function update(dt:Float, collide:DetectCollision, forceFriction:Bool = false):Void {
		if (lifePoints > 0) {
			this.acceleration.set(0, 0);
			velocity.x = Math.cos(this.direction) * 50 * projectileMoveSpeedFactor;
			velocity.y = Math.sin(this.direction) * 50 * projectileMoveSpeedFactor;
			this.rotation = this.direction;

			anim.run(anim -> {
				anim.loop = true;
				anim.play(SequenceKey.Iddle);
				anim.speed = 15 * dt / hxd.Timer.dt;
			});

			super.update(dt, collide, false);
		}
	}
}
