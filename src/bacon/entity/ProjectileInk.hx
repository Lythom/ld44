package bacon.entity;

import h3d.mat.Texture;
import bacon.spelling.Spell.MaterialKind;
import h2d.Graphics;
import bacon.Anims;
import platformer.BasicPhysicsObject;
import tools.anim.AnimatedSprite;
import h2d.col.Bounds;
import h2d.Object;
import h2d.Bitmap;
import bacon.entity.Entity.IEntity;

using Safety;

/**
 * ...
 * @author Lythom
 */
class ProjectileInk extends BasicPhysicsObject implements IEntity {
	public var hitbox(default, null):Bounds = Bounds.fromValues(0, 0, 0, 0);
	public var anim:AnimatedSprite;
	public var direction:Float = 1;
	public var lifePoints:Int = 1;
	public var material(default, set):MaterialKind = MaterialKind.Ink;
	public var shadow:Bitmap;

	var projectileMoveSpeedFactor:Float = 1;
	var paperPrintCooldown:Float = 0;

	function set_material(value:MaterialKind):MaterialKind {
		if (this.material != value) {
			this.anim.remove();
			updateAnimation(value);
			return this.material = value;
		}
		return this.material;
	}

	private inline function updateAnimation(material:MaterialKind) {
		var animData = AnimationData.data.get(material).sure();
		this.anim = new AnimatedSprite(animData.tiles, animData.sequences, 15, this);
	}

	public function new(x:Float = 0, y:Float = 0, direction:Int = 1, material:MaterialKind = MaterialKind.Ink, ?parent:Object) {
		super(x, y, parent);
		this.direction = direction;
		this.shadow = new Bitmap(hxd.Res.shadow.toTile());
		updateAnimation(material);
		this.material = material;
		fromData();
	}

	public function cleanup() {
		acceleration.set(0, 0);
		velocity.set(0, 0);
		maxVelocity.set(0, 0);
		x = 0;
		y = 0;
		anim.onAnimEnd = function() {};
		anim.currentFrame = 0;
		anim.transitionningFrom = null;
		anim.pause = false;
		syncPos();
	}

	public function fromData() {
		this.removeChildren();

		projectileMoveSpeedFactor = Data.config.get(Data.ConfigKind.projectileMoveSpeedFactor).sure().value;
		var projectileShadowOffset = Data.config.get(Data.ConfigKind.projectileShadowOffset).sure().value;

		// Refresh anim data
		updateAnimation(material);
		anim.play(SequenceKey.Iddle);

		shadow.tile.setCenterRatio();
		shadow.tile.dy = Std.int(shadow.tile.dy + projectileShadowOffset);
		shadow.setScale(anim.frames[0].width * 0.9 / shadow.tile.width);
		addChildAt(shadow, 0);

		this.hitbox = Bounds.fromValues(shadow.tile.dx * shadow.scaleX, shadow.tile.dy * shadow.scaleX, anim.frames[0].width * 0.8,
			anim.frames[0].width * 0.8 * shadow.tile.height / shadow.tile.width);

		// var bmpPerso:Bitmap = new Bitmap(h2d.Tile.fromColor(0xFFFFFF, Std.int(this.hitbox.width), Std.int(this.hitbox.height)), this);
		// bmpPerso.color = new h3d.Vector(1, 1, 1, 0.3);
		// bmpPerso.tile.dx = Std.int(this.hitbox.x);
		// bmpPerso.tile.dy = Std.int(this.hitbox.y);
	}

	public function updateMagink(dt:Float, collide:DetectCollision, canvas:Graphics, texture:Texture):Void {
		update(dt, collide);
		switch (material) {
			case Ink:
				if (Math.random() < dt / 3 && lifePoints > 0) {
					var w = 7 + Math.random() * 3;
					canvas.beginFill(Data.colors.get(Data.ColorsKind.COLD_DARK).sure().value);
					canvas.drawEllipse(this.x + this.hitbox.x + Math.random() * 20 - 10, this.y + this.hitbox.y + Math.random() * 20 - 10, w, w / 2);
					canvas.endFill();
					w = 5 + Math.random() * 3;
					canvas.beginFill(Data.colors.get(Data.ColorsKind.COLD_DARK).sure().value);
					canvas.drawEllipse(this.x + this.hitbox.x + Math.random() * 20 - 10, this.y + this.hitbox.y + Math.random() * 20 - 10, w, w / 2);
					canvas.endFill();
				}
			case Paper:
				paperPrintCooldown += dt;
				if (paperPrintCooldown >= 1 / 6) {
					paperPrintCooldown -= 1 / 6;
					anim.drawTo(texture);
				}
			default:
		}
	}

	public function destroy(onDestroy:ProjectileInk->Void, canvas:Graphics, texture:Texture) {
		anim.play(SequenceKey.Explode, 0);
		anim.loop = false;
		switch (material) {
			case Ink:
				for (i in 0...2) {
					var w = 12 + Math.random() * 15;
					canvas.beginFill(Data.colors.get(Data.ColorsKind.COLD_DARK).sure().value);
					canvas.drawEllipse(this.x + this.hitbox.x + Math.random() * 20 - 10, this.y + this.hitbox.y + Math.random() * 20 - 10, w, w / 2);
					canvas.endFill();
				}
				anim.onAnimEnd = () -> {
					for (i in 0...6) {
						var w = 5 + Math.random() * 10;
						canvas.beginFill(Data.colors.get(Data.ColorsKind.COLD_DARK).sure().value);
						canvas.drawEllipse(this.x + this.hitbox.x + Math.random() * 60 - 30, this.y + this.hitbox.y + Math.random() * 60 - 30, w, w / 2);
						canvas.endFill();
					}
					onDestroy(this);
				}
			case Paper:
				anim.onAnimEnd = () -> {
					// this.drawTo(texture);
					TDO debug here why doesn't it print final frame ?
					onDestroy(this);
				}
			default:
		}
	}

	override public function update(dt:Float, collide:DetectCollision, forceFriction:Bool = false):Void {
		if (lifePoints > 0) {
			this.acceleration.set(0, 0);

			var speed = switch (material) {
				case Ink: 400;
				case Paper: 100;
				default: 400;
			}

			velocity.x = Math.cos(this.direction) * speed * projectileMoveSpeedFactor;
			velocity.y = Math.sin(this.direction) * speed * projectileMoveSpeedFactor;

			anim.loop = true;
			anim.play(SequenceKey.Iddle);
			anim.speed = 15 * dt / hxd.Timer.dt;
			anim.rotation = this.direction;

			this.shadow.visible = material != Paper;

			super.update(dt, collide, false);
		}
	}
}
