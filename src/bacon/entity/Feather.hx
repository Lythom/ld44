package bacon.entity;

import h2d.Text;
import h2d.Object;

using tweenxcore.Tools;
using Safety;

class Feather extends h2d.Bitmap {
	var currentAnim:String = "";
	var acc:Float = 0;
	var spellWidth:Float = 0;
	var cb:Null<Void->Void> = null;
	var perlin:tools.Perlin = new tools.Perlin();

	public function new(?parent:h2d.Object) {
		super(hxd.Res.plume.toTile(), parent);
		tile.dx = 0;
		tile.dy = 0;
	}

	public function update(dt:Float, tf:Text) {
		acc += dt;
		if (currentAnim == "wrong") {
			var duration = 0.3;
			var progress = (acc / duration);
			tile.dx = Std.int(wrongEase(progress) * spellWidth);
			if (progress >= 1) {
				resetToDefaults();
				this.cb.run(cb -> cb());
			}
		} else if (currentAnim == "casting") {
			var duration = 0.3;
			var progress = (acc / duration);
			tile.dx = Std.int(spellWidth - progress.cubicOutIn() * spellWidth);
			tile.dy = -Std.int(castEase(progress) * 100);
			this.rotation = castEase(progress) * 0.6;
			if (progress >= 1) {
				resetToDefaults();
				this.cb.run(cb -> cb());
			}
		} else {
			var p = 0.5 - perlin.OctavePerlin(this.x * 0.05, 0, 0, 3, 0.5, 2);
			tile.dy = p * 100;
		}

		var nextX = tf.x + tf.textWidth * tf.scaleX;
		this.x = tf.textWidth == 0 ? tf.x : (0.15).lerp(this.x, nextX);
		this.y = tf.y - tile.height / 2 - 10;
	}

	function resetToDefaults() {
		tile.dx = 0;
		tile.dy = 0;
		this.rotation = 0;
		currentAnim = "";
	}

	public function playWrong(spellWidth:Float, ?onFinish:Void->Void) {
		this.spellWidth = spellWidth;
		currentAnim = "wrong";
		acc = 0;
		this.cb = onFinish;
	}

	public function playCast(spellWidth:Float, ?onFinish:Void->Void) {
		this.spellWidth = spellWidth;
		currentAnim = "casting";
		acc = 0;
		this.cb = onFinish;
	}

	// http://tweenx.spheresofa.net/core/custom/#%7B%22time%22%3A0.25%2C%22easing%22%3A%5B%22Op%22%2C%5B%22Op%22%2C%5B%22Simple%22%2C%5B%22Standard%22%2C%22Quad%22%2C%22InOut%22%5D%5D%2C%5B%22RoundTrip%22%2C%22Yoyo%22%5D%5D%2C%5B%22Repeat%22%2C1.5%5D%5D%7D
	function wrongEase(rate:Float):Float {
		return tweenxcore.Tools.FloatTools.yoyo(tweenxcore.Tools.FloatTools.repeat(tweenxcore.Tools.FloatTools.lerp(rate, 0, 1.5), 0, 1),
			tweenxcore.Tools.Easing.quadInOut);
	}

	function castEase(rate:Float):Float {
		return tweenxcore.Tools.FloatTools.zigzag(rate, tweenxcore.Tools.Easing.quadOut);
	}
}
