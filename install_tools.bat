start "Choose 4.0.0-rc2" "https://haxe.org/download/version/4.0.0-rc2/"
start "Choose latest" "https://hashlink.haxe.org/#download"
start "Choose version 1.5" "http://castledb.org/"
start "Choose version 0.5.1" "https://hxscout.com/download.html"
start "Choose latest" "https://code.visualstudio.com/"

haxelib install compile.hxml